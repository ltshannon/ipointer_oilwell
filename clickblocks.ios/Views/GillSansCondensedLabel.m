//
//  GillSansCondensedLabel.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/7/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "GillSansCondensedLabel.h"
#import "UIHelper.h"

@implementation GillSansCondensedLabel

- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = [UIFont fontWithName:kGillSansStdCondensed size:self.font.pointSize];
}

@end
