//
//  CurrentLocationAnnotationView.m
//  OilWell
//
//  Created by Nikita Rosenberg on 2/8/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//




#import "CurrentLocationAnnotationView.h"



@implementation CurrentLocationAnnotationView



- (id)initWithAnnotation:(id<MKAnnotation>)annotation reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithAnnotation:annotation reuseIdentifier:reuseIdentifier];
    if (self)
    {
        self.image = [UIImage imageNamed:@"currentLocationAnnotation.png"];
        // Set the frame size to the appropriate values.
        CGRect myFrame = self.frame;
        myFrame.size.width = self.image.size.width;
        myFrame.size.height = self.image.size.height;
        self.frame = myFrame;
        self.opaque = NO;
    }
    return self;
}



@end
