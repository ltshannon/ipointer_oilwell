//
//  GillSansBoldCondensedLabel.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/7/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "GillSansBoldCondensedLabel.h"
#import "UIHelper.h"

@implementation GillSansBoldCondensedLabel

- (void)awakeFromNib {
    [super awakeFromNib];
    self.font = [UIFont fontWithName:kGillSansStdBoldCondensed size:self.font.pointSize];
}

@end
