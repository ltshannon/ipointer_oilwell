//
//  AppDelegate.h
//  clickblocks.ios
//
//  Created by Vitaly Dubov on 9/27/12.
//  Copyright (c) 2012 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "OilWellDB.h"
#import "LoginViewController.h"

@class ViewController;

@interface AppDelegate : UIResponder <UIApplicationDelegate>
{
    OilWellDB *oilWellDB;
}

@property (strong, nonatomic) UIWindow *window;

@property (strong, nonatomic) ViewController        *viewController;
@property (strong ,nonatomic) NSMutableArray        *timeStrings;
@property (strong, nonatomic) NSArray               *allLinedStrings;
@property (strong, nonatomic) LoginViewController   *loginViewController;
@property int32_t                                   apiCount;

@end
