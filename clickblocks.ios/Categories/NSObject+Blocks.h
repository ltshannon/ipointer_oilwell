//
//  NSObject+Blocks.h
//  MyPersonalBest
//
//  Created by Vitaly Dubov on 7/13/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSObject (Blocks)

- (void)performBlock:(void (^)(void))block afterDelay:(NSTimeInterval)delay;

@end
