//
//  NSString+Additions.h
//  Spot411
//
//  Created by Matt Long on 3/6/10.
//  Copyright 2010 Spot411 Holdings, LLC.. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString(Additions)

- (NSString*)timeString;
- (NSString*)urlencode;
- (NSString *)urldecode;
- (NSString*)md5;
- (NSString*)localized;
- (NSString*)trim;

@end
