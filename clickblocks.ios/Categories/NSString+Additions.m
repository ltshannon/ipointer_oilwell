//
//  NSString+Additions.m
//  Spot411
//
//  Created by Matt Long on 3/6/10.
//  Copyright 2010 Spot411 Holdings, LLC.. All rights reserved.
//

#import "NSString+Additions.h"


NSString *_mlfilterChars = @"!*'();:@&=+$,/?%#[]";

@implementation NSString(Additions)

- (NSString*)timeString;
{
    int offset = [self intValue];
    
    int totalMinutes = offset / 60;
    int hours = totalMinutes / 60;
    int minutes = totalMinutes % 60;
    int seconds = offset % 60;
    
    return [NSString stringWithFormat:@"%02d:%02d:%02d", hours, minutes, seconds];
}


- (NSString*)urlencode
{
    return [[NSString stringWithString: (NSString *)
             CFURLCreateStringByAddingPercentEscapes(
                                                     NULL, 
                                                     (CFStringRef)self,
                                                     NULL, 
                                                     (CFStringRef)_mlfilterChars,
                                                     kCFStringEncodingUTF8)]
            stringByReplacingOccurrencesOfString: @"%20" withString: @"+"];
}

- (NSString *)urldecode
{
    NSString *result = [(NSString *)self stringByReplacingOccurrencesOfString:@"+" withString:@" "];
    result = [result stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    return result;
}

- (NSString*)md5
{
    const char *cStr = [self UTF8String];
    unsigned char result[16];
    CC_MD5( cStr, strlen(cStr), result );
    return [NSString stringWithFormat:
            @"%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x%02x",
            result[0], result[1], result[2], result[3], 
            result[4], result[5], result[6], result[7],
            result[8], result[9], result[10], result[11],
            result[12], result[13], result[14], result[15]
            ];  
}


- (NSString*)localized;
{
    return NSLocalizedString(self, nil);
}

- (NSString*)trim {
    return [self stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

@end
