//
//  UIImage+Scale.h
//  test_favicon
//
//  Created by Ilya Sedov on 24.08.11.
//  Copyright 2011 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIImage (UIImage_Scale)

- (UIImage *) scaleToSize: (CGSize)size;
- (UIImage *) scaleProportionalToSize: (CGSize)size1;
- (UIImage *)fixOrientation;

@end
