//
//  NSString+Base64.m
//  TVPlus
//
//  Created by Vitaliy Dubov on 13.05.11.
//  Copyright 2011 Saritasa. All rights reserved.
//

#import "NSString+Base64.h"
#import "NSData+Base64.h"

@implementation NSString (Base64)

- (NSString *) toBase64 {
	return [[self dataUsingEncoding:NSUTF8StringEncoding] base64Encoding];
}

- (NSString *) fromBase64 {
	return [[[NSString alloc] initWithData:[NSData dataWithBase64EncodedString:self] encoding:NSUTF8StringEncoding] autorelease];
}

@end
