//
//  UILabel+VerticalAlign.h
//  TVPlus
//
//  Created by Vitaliy Dubov on 26.04.11.
//  Copyright 2011 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface UILabel (VerticalAlign)
- (void)alignTop;
- (void)alignBottom;
@end
