//
//  NSString+Base64.h
//  TVPlus
//
//  Created by Vitaliy Dubov on 13.05.11.
//  Copyright 2011 Saritasa. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Base64)

- (NSString *) toBase64;
- (NSString *) fromBase64;

@end
