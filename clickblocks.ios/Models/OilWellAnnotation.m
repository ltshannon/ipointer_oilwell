//
//  OilWellAnnotation.m
//  OilWell
//
//  Created by Nikita Rosenberg on 2/8/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "OilWellAnnotation.h"

@implementation OilWellAnnotation

@synthesize coordinate;

- (id) initWithLocation:(CLLocationCoordinate2D)pCoordinate wellData: (NSDictionary *) wellData
{
    self = [super init];
    if ( self )
    {
        coordinate = pCoordinate;
        self.wellData = wellData;
    }
    return self;
}

- (NSString *)title
{
    NSString *leaseName = [[self.wellData objectForKey:@"well"] objectForKey:@"LeaseName"];
    NSString *wellNumber = [[self.wellData objectForKey:@"well"] objectForKey:@"WellNumber"];
    return [NSString stringWithFormat:@"%@ %@", leaseName, wellNumber];
}

@end
