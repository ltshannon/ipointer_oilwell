//
//  CurrentLocationAnnotation.h
//  OilWell
//
//  Created by Nikita Rosenberg on 2/8/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//




#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>




@interface CurrentLocationAnnotation : NSObject
<MKAnnotation>
{
    CLLocationCoordinate2D coordinate;
}


@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;

- (id) initWithLocation:(CLLocationCoordinate2D)coordinate;

@end
