//
//  AuditAnswerDB.m
//  OilWell
//
//  Created by LAWRENCE SHANNON on 9/28/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "AuditAnswerDB.h"

@implementation AuditAnswerDB

- (id) initialise
{
	if(![self openDatabase])
	{
		//NSLog(@"Open database failed.");
	}
    //NSLog(@"Open database");
    
	return self;
}

- (BOOL) openDatabase
{
    if (!database)
    {
        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/AuditAnswer.db"];
        int result = sqlite3_open([path UTF8String], &database);
        if (result != SQLITE_OK)
        {
            NSLog(@"Audit Answer DB Open/Create failed!");
            return NO;
        }
//        const char *sql = "create table if not exists settings_table (serverURL text, userName text, password text);";
        const char *sql = "CREATE TABLE IF NOT EXISTS WellAudit (ID INTEGER PRIMARY KEY, wellAPI varchar(32), createdBy INTEGER, created timestamp, comments text)";
        char *errMsg = NULL;
        result = sqlite3_exec(database, sql, NULL, NULL, &errMsg);
        if (result != SQLITE_OK)
        {
            NSLog(@"Create DB Settings failed!");
            NSLog(@"Error : %@", [NSString stringWithUTF8String:errMsg]);
            return NO;
        }
    }
    
    return YES;
    
}

- (BOOL) getSettingsData
{
    
    NSString *query=[NSString stringWithFormat:@"SELECT * FROM settings_table"];
    
    sqlite3_stmt *stmt;
    int result = sqlite3_prepare(database, [query UTF8String], -1, &stmt, NULL);
    if (result != SQLITE_OK)
    {
        NSLog(@"Database sqlite prepare for failed!");
        return NO;
    }
    
    if (sqlite3_step(stmt) == SQLITE_ROW)
    {
//        self.serverURL = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 0)];
//        self.userName = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 1)];
//        self.password = [NSString stringWithUTF8String:(char *)sqlite3_column_text(stmt, 2)];
        sqlite3_finalize(stmt);
        return YES;
    }
    
    sqlite3_finalize(stmt);
    return NO;
}

- (void) saveSettingsData: (NSString *) serverURL userName: (NSString *) username password: (NSString *) password
{
    
    char *errMsg = NULL;
    NSString *query=[NSString stringWithFormat:@"INSERT INTO settings_table(serverURL, userName, password) VALUES ('%@', '%@', '%@')", serverURL, username, password];
    
    if (sqlite3_exec(database, [@"DELETE From settings_table;" UTF8String], NULL, NULL, &errMsg) != SQLITE_OK)
    {
        NSLog(@"Error: %s", errMsg);
    }
    
    int result = sqlite3_exec(database, [query UTF8String], NULL, NULL, &errMsg);
    
    if (result != SQLITE_OK)
    {
        NSLog(@"SQL insert item: %@/n", query);
        NSLog(@"Error : %@/n", [NSString stringWithUTF8String:errMsg]);
    }
    
    return;
    
}

@end
