//
//  OilWellDB.h
//  OilWell
//
//  Created by LAWRENCE SHANNON on 10/14/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface OilWellDB : NSObject
{
    sqlite3 *database;
}

-(id) initialise;
-(BOOL)openDatabase;
- (void) insertDataLookupChemicalProperties: (int64_t) propertyID name: (NSString *) name chemicalType: (NSString *) chemicalType;
- (void) insertDataWellAudit: (int64_t) id
                     wellAPI: (NSString *) wellAPI
                   createdBy: (int64_t) createdBy
                     created: (NSString *) created
                    comments: (NSString *) comments
                     auditID: (int64_t) auditID
                   APINumber: (NSString *) APINumber
                chemicalType: (NSString *) chemicalType
                    property: (NSString *) property
                   userEmail: (NSString *) userEmail
                    datetime: (NSString *) datetime;

- (void) insertDataWellAuditProperties: (int64_t) auditID
                            propertyID: (int64_t) propertyID
                               success: (int64_t) success
                               comment: (NSString *) comment;
- (void) insertWells: (void *) none
          DistrictNu: (NSString *) DistrictNu
           APINumber: (NSString *) APINumber
             BLMWell: (NSString *) BLMWell
          RedrillCan: (NSString *) RedrillCan
             DryHole: (NSString *) DryHole
          WellStatus: (NSString *) WellStatus
          OperatorNa: (NSString *) OperatorNa
          CountyName: (NSString *) CountyName
           FieldName: (NSString *) FieldName
            AreaName: (NSString *) AreaName
             section:(double) section
            Township: (NSString *) Township
               Range: (NSString *) Range
          BaseMeridi: (NSString *) BaseMeridi
           Elevation: (NSString *) Elevation
          LocationDe: (double) LocationDe
            Latitude: (double) Latitude
           Longitude: (double) Longitude
          GISSourceC: (NSString *) GISSourceC
           LeaseName: (NSString *) LeaseName
          WellNumber: (NSString *) WellNumber
             EPAWell: (NSString *) EPAWell
          Hydraulica: (NSString *) Hydraulica
          Confidenti: (NSString *) Confidenti
            SPUDDate: (NSString *) SPUDDate
          WellDepthA: (NSString *) WellDepthA
          RedrillFoo: (NSString *) RedrillFoo
          AbandonedD: (NSString *) AbandonedD
          Completion: (NSString *) Completion
           GISSymbol: (NSString *) GISSymbol
              API_10: (NSString *) API_10
             OS_AREA: (NSString *) OS_AREA
                 GPS: (double) GPS
                WELL: (NSString *) WELL
         WELL_STATUS: (NSString *) WELL_STATUS
            CHEMICAL: (NSString *) CHEMICAL
     SETUP_CONDITION: (NSString *) SETUP_CONDITION
              LABELS: (NSString *) LABELS
       VISABLE_LEAKS: (NSString *) VISABLE_LEAKS
  CONTAIN_MENT_CLEAN: (NSString *) CONTAIN_MENT_CLEAN
       ELECTRIC_SAFE: (NSString *) ELECTRIC_SAFE
  NO_TRIPPING_HAZARD: (NSString *) NO_TRIPPING_HAZARD
                DATE: (NSString *) DATE
            COMMENTS: (NSString *) COMMENTS
           AuditUser: (NSString *) AuditUser;

@end
