//
//  CurrentLocationAnnotation.m
//  OilWell
//
//  Created by Nikita Rosenberg on 2/8/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//




#import "CurrentLocationAnnotation.h"



@implementation CurrentLocationAnnotation


@synthesize coordinate;


- (id) initWithLocation:(CLLocationCoordinate2D)pCoordinate
{
    self = [super init];
    if ( self )
    {
        coordinate = pCoordinate;
    }
    return self;
}




- (NSString *)title
{
    return @"You";
}




@end
