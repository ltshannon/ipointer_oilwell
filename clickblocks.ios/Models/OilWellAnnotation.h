//
//  OilWellAnnotation.h
//  OilWell
//
//  Created by Nikita Rosenberg on 2/8/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//




#import <CoreLocation/CoreLocation.h>
#import <Foundation/Foundation.h>




@interface OilWellAnnotation : NSObject<MKAnnotation>
{
    CLLocationCoordinate2D coordinate;
}


@property (nonatomic, readonly) CLLocationCoordinate2D              coordinate;
@property (retain, nonatomic) NSDictionary                          *wellData;

- (id) initWithLocation:(CLLocationCoordinate2D)coordinate wellData: (NSDictionary *) wellData;

@end
