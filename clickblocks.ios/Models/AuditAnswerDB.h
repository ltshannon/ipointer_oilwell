//
//  AuditAnswerDB.h
//  OilWell
//
//  Created by LAWRENCE SHANNON on 9/28/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <sqlite3.h>

@interface AuditAnswerDB : NSObject
{
    sqlite3 *database;
}

-(id) initialise;
-(BOOL)openDatabase;
- (BOOL) getSettingsData;
- (void) saveSettingsData: (NSString *) serverURL userName: (NSString *) username password: (NSString *) password;

@end
