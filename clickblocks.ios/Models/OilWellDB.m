//
//  OilWellDB.m
//  OilWell
//
//  Created by LAWRENCE SHANNON on 10/14/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "OilWellDB.h"

@implementation OilWellDB

- (id) initialise
{
	if(![self openDatabase])
	{
		//NSLog(@"Open database failed.");
	}
    //NSLog(@"Open database");
    
	return self;
}

- (BOOL) openDatabase
{
    if (!database)
    {
        NSString *path = [NSHomeDirectory() stringByAppendingPathComponent:@"Documents/OilWellDB.db"];
        int result = sqlite3_open([path UTF8String], &database);
        if (result != SQLITE_OK)
        {
            NSLog(@"LookupChemicalProperties DB Open/Create failed!");
            return NO;
        }
        const char *sql = "CREATE TABLE IF NOT EXISTS LookupChemicalProperties (propertyID INTEGER PRIMARY KEY AUTOINCREMENT, name varchar(255) not NULL, chemicalType varchar(50) not NULL)";
        char *errMsg = NULL;
        result = sqlite3_exec(database, sql, NULL, NULL, &errMsg);
        if (result != SQLITE_OK)
        {
            NSLog(@"Create DB Settings failed!");
            NSLog(@"Error : %@", [NSString stringWithUTF8String:errMsg]);
            return NO;
        }

        sql = "CREATE TABLE IF NOT EXISTS WellAudit (ID INTEGER PRIMARY KEY AUTOINCREMENT, wellAPI text not NULL, createdBy INTEGER not NULL, created text not null, comments text, auditID INTEGER not null, APINumber text not null, chemicalType text not null, property text not null, userEmail text not null, datetime text not null)";
        errMsg = NULL;
        result = sqlite3_exec(database, sql, NULL, NULL, &errMsg);
        if (result != SQLITE_OK)
        {
            NSLog(@"Create DB Settings failed!");
            NSLog(@"Error : %@", [NSString stringWithUTF8String:errMsg]);
            return NO;
        }
    
        sql = "CREATE TABLE IF NOT EXISTS WellAuditProperties (auditID INTEGER PRIMARY KEY not null, property ID INTEGER not null, success INTEGER not null, comments text)";
        errMsg = NULL;
        result = sqlite3_exec(database, sql, NULL, NULL, &errMsg);
        if (result != SQLITE_OK)
        {
            NSLog(@"Create DB Settings failed!");
            NSLog(@"Error : %@", [NSString stringWithUTF8String:errMsg]);
            return NO;
        }

        sql = "CREATE TABLE IF NOT EXISTS Wells ( \
            DistrictNu text, \
            APINumber text, \
            BLMWell text, \
            RedrillCan text, \
            DryHole text, \
            WellStatus text, \
            OperatorNa text, \
            CountyName text, \
            FieldName text, \
            AreaName text, \
            Section real, \
            Township text, \
            Range text, \
            BaseMeridi text, \
            Elevation text, \
            LocationDe real, \
            Latitude real, \
            Longitude real, \
            GISSourceC text, \
            LeaseName text, \
            WellNumber text, \
            EPAWell text, \
            Hydraulica text, \
            Confidenti text, \
            SPUDDate text, \
            WellDepthA text, \
            RedrillFoo text, \
            AbandonedD text, \
            Completion text, \
            GISSymbol text, \
            API_10 real, \
            OS_AREA text, \
            GPS real, \
            WELL text, \
            WELL_STATUS text, \
            CHEMICAL text, \
            SETUP_CONDITION text, \
            LABELS text, \
            VISABLE_LEAKS text, \
            CONTAIN_MENT_CLEAN text, \
            ELECTRIC_SAFE text, \
            NO_TRIPPING_HAZARD text, \
            DATE text, \
            COMMENTS text, \
            AuditUser text)";

        errMsg = NULL;
        result = sqlite3_exec(database, sql, NULL, NULL, &errMsg);
        if (result != SQLITE_OK)
        {
            NSLog(@"Create DB Settings failed!");
            NSLog(@"Error : %@", [NSString stringWithUTF8String:errMsg]);
            return NO;
        }


    }
    
    return YES;
    
}

- (void) insertDataLookupChemicalProperties: (int64_t) propertyID name: (NSString *) name chemicalType: (NSString *) chemicalType
{
    
    char *errMsg = NULL;
    NSString *query=[NSString stringWithFormat:@"INSERT INTO LookupChemicalProperties(propertyID, name, chemicalType) VALUES (%lld, '%@', '%@')", propertyID, name, chemicalType];
    
    int result = sqlite3_exec(database, [query UTF8String], NULL, NULL, &errMsg);
    
    if (result != SQLITE_OK)
    {
        NSLog(@"SQL insert item: %@/n", query);
        NSLog(@"Error : %@/n", [NSString stringWithUTF8String:errMsg]);
    }
    
    return;
    
}

- (void) insertDataWellAudit: (int64_t) id
                     wellAPI: (NSString *) wellAPI
                   createdBy: (int64_t) createdBy
                     created: (NSString *) created
                    comments: (NSString *) comments
                     auditID: (int64_t) auditID
                   APINumber: (NSString *) APINumber
                chemicalType: (NSString *) chemicalType
                    property: (NSString *) property
                   userEmail: (NSString *) userEmail
                    datetime: (NSString *) dt
{
    
    char *errMsg = NULL;
    NSString *query=[NSString stringWithFormat:@"INSERT INTO WellAudit (ID, wellAPI, createdBy, created, comments, auditID, APINumber, chemicalType, property, userEmail, datetime) VALUES (%lld, '%@', %lld, '%@', '%@', %lld, '%@', '%@', '%@', '%@', '%@')", id, wellAPI, createdBy, created, comments, auditID, APINumber, chemicalType, property, userEmail, dt];

    
    int result = sqlite3_exec(database, [query UTF8String], NULL, NULL, &errMsg);
    
    if (result != SQLITE_OK)
    {
        NSLog(@"SQL insert item: %@/n", query);
        NSLog(@"Error : %@/n", [NSString stringWithUTF8String:errMsg]);
    }
    
    return;
    
}

- (void) insertDataWellAuditProperties: (int64_t) auditID
                            propertyID: (int64_t) propertyID
                               success: (int64_t) success
                               comment: (NSString *) comment
{
    
    char *errMsg = NULL;
    NSString *query=[NSString stringWithFormat:@"INSERT INTO WellAuditProperties (auditID, propertyID, success, comment) VALUES (%lld, '%lld, %lld, '%@')", auditID, propertyID, success, comment];
    
    
    int result = sqlite3_exec(database, [query UTF8String], NULL, NULL, &errMsg);
    
    if (result != SQLITE_OK)
    {
        NSLog(@"SQL insert item: %@/n", query);
        NSLog(@"Error : %@/n", [NSString stringWithUTF8String:errMsg]);
    }
    
    return;
    
}

- (void) insertWells: (void *) none
          DistrictNu: (NSString *) DistrictNu
           APINumber: (NSString *) APINumber
             BLMWell: (NSString *) BLMWell
          RedrillCan: (NSString *) RedrillCan
             DryHole: (NSString *) DryHole
          WellStatus: (NSString *) WellStatus
          OperatorNa: (NSString *) OperatorNa
          CountyName: (NSString *) CountyName
           FieldName: (NSString *) FieldName
            AreaName: (NSString *) AreaName
             section:(double) section
            Township: (NSString *) Township
               Range: (NSString *) Range
          BaseMeridi: (NSString *) BaseMeridi
           Elevation: (NSString *) Elevation
          LocationDe: (double) LocationDe
            Latitude: (double) Latitude
           Longitude: (double) Longitude
          GISSourceC: (NSString *) GISSourceC
           LeaseName: (NSString *) LeaseName
          WellNumber: (NSString *) WellNumber
             EPAWell: (NSString *) EPAWell
          Hydraulica: (NSString *) Hydraulica
          Confidenti: (NSString *) Confidenti
            SPUDDate: (NSString *) SPUDDate
          WellDepthA: (NSString *) WellDepthA
          RedrillFoo: (NSString *) RedrillFoo
          AbandonedD: (NSString *) AbandonedD
          Completion: (NSString *) Completion
           GISSymbol: (NSString *) GISSymbol
              API_10: (NSString *) API_10
             OS_AREA: (NSString *) OS_AREA
                 GPS: (double) GPS
                WELL: (NSString *) WELL
         WELL_STATUS: (NSString *) WELL_STATUS
            CHEMICAL: (NSString *) CHEMICAL
     SETUP_CONDITION: (NSString *) SETUP_CONDITION
              LABELS: (NSString *) LABELS
       VISABLE_LEAKS: (NSString *) VISABLE_LEAKS
  CONTAIN_MENT_CLEAN: (NSString *) CONTAIN_MENT_CLEAN
       ELECTRIC_SAFE: (NSString *) ELECTRIC_SAFE
  NO_TRIPPING_HAZARD: (NSString *) NO_TRIPPING_HAZARD
                DATE: (NSString *) DATE
            COMMENTS: (NSString *) COMMENTS
           AuditUser: (NSString *) AuditUser
{
    
    char *errMsg = NULL;
    NSString *query=[NSString stringWithFormat:@"INSERT INTO Wells ( \
                                    DistrictNu, \
                                     APINumber, \
                                       BLMWell, \
                                    RedrillCan, \
                                       DryHole, \
                                    WellStatus, \
                                    OperatorNa, \
                                    CountyName, \
                                     FieldName, \
                                      AreaName, \
                                       section, \
                                      Township, \
                                         Range, \
                                    BaseMeridi, \
                                     Elevation, \
                                    LocationDe, \
                                      Latitude, \
                                     Longitude, \
                                    GISSourceC, \
                                     LeaseName, \
                                    WellNumber, \
                                       EPAWell, \
                                    Hydraulica, \
                                    Confidenti, \
                                      SPUDDate, \
                                    WellDepthA, \
                                    RedrillFoo, \
                                    AbandonedD, \
                                    Completion, \
                                    GISSymbol, \
                                        API_10, \
                                       OS_AREA, \
                                           GPS, \
                                          WELL, \
                                   WELL_STATUS, \
                                      CHEMICAL, \
                               SETUP_CONDITION, \
                                        LABELS, \
                                 VISABLE_LEAKS, \
                            CONTAIN_MENT_CLEAN, \
                                 ELECTRIC_SAFE, \
                            NO_TRIPPING_HAZARD, \
                                          DATE, \
                                      COMMENTS, \
                                     AuditUser) Values ('%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', %f, '%@', '%@', '%@', '%@', %f, %f, %f, '%@', '%@', \
                                                        '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', '%@', %f, '%@', '%@', '%@', '%@', '%@', '%@', '%@', \
                                                        '%@', '%@', '%@', '%@', '%@')",
                     DistrictNu,
                     APINumber,
                     BLMWell,
                     RedrillCan,
                     DryHole,
                     WellStatus,
                     OperatorNa,
                     CountyName,
                     FieldName,
                     AreaName,
                     section,
                     Township,
                     Range,
                     BaseMeridi,
                     Elevation,
                     LocationDe,
                     Latitude,
                     Longitude,
                     GISSourceC,
                     LeaseName,
                     WellNumber,
                     EPAWell,
                     Hydraulica,
                     Confidenti,
                     SPUDDate,
                     WellDepthA,
                     RedrillFoo,
                     AbandonedD,
                     Completion,
                     GISSymbol,
                     API_10,
                     OS_AREA,
                     GPS,
                     WELL,
                     WELL_STATUS,
                     CHEMICAL,
                     SETUP_CONDITION,
                     LABELS,
                     VISABLE_LEAKS,
                     CONTAIN_MENT_CLEAN,
                     ELECTRIC_SAFE,
                     NO_TRIPPING_HAZARD,
                     DATE,
                     COMMENTS,
                     AuditUser];

    int result = sqlite3_exec(database, [query UTF8String], NULL, NULL, &errMsg);
    
    if (result != SQLITE_OK)
    {
        NSLog(@"SQL insert item: %@/n", query);
        NSLog(@"Error : %@/n", [NSString stringWithUTF8String:errMsg]);
    }
    
    return;
    
}


@end
