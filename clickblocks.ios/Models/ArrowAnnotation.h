//
//  ArrowAnnotation.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/26/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>

@interface ArrowAnnotation : NSObject <MKAnnotation> {
    
}

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, assign) CLLocationDirection heading;

- (id) initWithLocation:(CLLocationCoordinate2D)coordinate;

@end
