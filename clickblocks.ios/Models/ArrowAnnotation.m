//
//  ArrowAnnotation.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/26/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "ArrowAnnotation.h"


@implementation ArrowAnnotation

- (id) initWithLocation:(CLLocationCoordinate2D)pCoordinate
{
    self = [super init];
    if ( self )
    {
        _coordinate = pCoordinate;
    }
    return self;
}

- (NSString *)title
{
    return [NSString stringWithFormat:@"Heading %.2f", _heading];
}

@end
