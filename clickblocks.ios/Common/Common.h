//
//  Common.h
//  Monex
//
//  Created by Vitaly Dubov on 9/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AppDelegate;
@class ViewController;
@interface Common : NSObject

+ (AppDelegate *) appDelegate;
+ (ViewController *) rootController;
+ (UIImage *) imageNamed: (NSString *) imageName;
+ (NSString *) documentsDir;
+ (void) forceDirectories: (NSString *) path;
+ (void) printAvailableFonts;
+ (void) importFonts;
+ (NSString *) stringFromFile: (NSString *) filename;
+ (NSDate *) dateFromDotNetJSONString:(NSString *)string;
+ (NSString*) stringFromNumber:(NSNumber*)number;
+ (NSString*) stringFromNumber:(NSNumber*)number withGroupSize:(NSInteger)group;

+ (NSArray*) splitStringIntoTwoLines:(NSString*)string withFont:(UIFont*)font constrainedToWidth:(float)width;
+ (NSString*) trimString:(NSString*) trimmedString;

+ (NSDate *) dateToLocalDate: (NSDate *) date;
+ (NSDate *) dateToGMTDate: (NSDate *) date;

@end
