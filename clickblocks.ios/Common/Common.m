//
//  Common.m
//  Monex
//
//  Created by Vitaly Dubov on 9/21/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "Common.h"
#import "AppDelegate.h"
#import "ViewController.h"
#import "DataManager.h"
#import "Constants.h"
#import <dlfcn.h>
#import <QuartzCore/QuartzCore.h>

@implementation Common

+ (AppDelegate *) appDelegate {
    return (AppDelegate *)[[UIApplication sharedApplication] delegate];
}

+ (ViewController *) rootController {
    return [[[self class] appDelegate] viewController];
}

+ (NSString *) documentsDir {
	return [NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES) objectAtIndex:0];	
}

+ (void) forceDirectories: (NSString *) path{
	NSError *err = nil;
	
	if(![[NSFileManager defaultManager] fileExistsAtPath:path isDirectory:NULL])
		[[NSFileManager defaultManager] createDirectoryAtPath:path withIntermediateDirectories:YES attributes:nil error:&err];
	
	if(err!=nil)
		DLog(@"Error creating dir: %@ - %@", path, [err localizedDescription]);
}

+ (UIImage *) imageNamed: (NSString *) imageName{
	NSString *filePath = [[NSBundle mainBundle] pathForResource:imageName ofType:@""]; 
	UIImage *image = [UIImage imageWithContentsOfFile:filePath];
	return image;
}

+ (void) printAvailableFonts {
    NSMutableDictionary *dict = [NSMutableDictionary dictionary];
    NSArray *familyNames = [UIFont familyNames];
    for (NSString *family in familyNames) {
        NSArray *fonts = [UIFont fontNamesForFamilyName:family];
        if (fonts) {
            [dict setObject:fonts forKey:family];
        }
    }
    
    DLog(@"fonts = %@", dict);
}

+ (void) importFonts {
    BOOL GSFontAddFromFile(const char * path);
	NSUInteger newFontCount = 0;
	NSBundle *frameworkBundle = [NSBundle bundleWithIdentifier:@"com.apple.GraphicsServices"];
	const char *frameworkPath = [[frameworkBundle executablePath] UTF8String];
	if (frameworkPath) {
		void *graphicsServices = dlopen(frameworkPath, RTLD_NOLOAD | RTLD_LAZY);
		if (graphicsServices) {
			BOOL (*GSFontAddFromFile)(const char *) = dlsym(graphicsServices, "GSFontAddFromFile");
			if (GSFontAddFromFile) {
				for (NSString *fontFile in [[NSBundle mainBundle] pathsForResourcesOfType:@"ttf" inDirectory:nil])
					newFontCount += GSFontAddFromFile([fontFile UTF8String]);
                
                for (NSString *fontFile in [[NSBundle mainBundle] pathsForResourcesOfType:@"otf" inDirectory:nil])
					newFontCount += GSFontAddFromFile([fontFile UTF8String]);
            }
		}
	}
}

+ (NSString *) stringFromFile: (NSString *) filename {
    NSError *error = nil;
    NSString *path = [[NSBundle mainBundle] pathForResource:filename ofType:nil];
    NSString *str = [NSString stringWithContentsOfFile:path encoding:NSASCIIStringEncoding error:&error];
    if (error) {
        DLog(@"%@", error);
    }
    return str;
}

+ (NSDate *) dateFromDotNetJSONString:(NSString *)string {
    if (!string || [string isKindOfClass:[NSNull class]]) return nil;
    
    static NSRegularExpression *dateRegEx = nil;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        dateRegEx = [[NSRegularExpression alloc] initWithPattern:@"^\\/date\\((-?\\d++)(?:([+-])(\\d{2})(\\d{2}))?\\)\\/$" options:NSRegularExpressionCaseInsensitive error:nil];
    });
    NSTextCheckingResult *regexResult = [dateRegEx firstMatchInString:string options:0 range:NSMakeRange(0, [string length])];
    
    if (regexResult) {
        // milliseconds
        NSTimeInterval seconds = [[string substringWithRange:[regexResult rangeAtIndex:1]] doubleValue] / 1000.0;
        // timezone offset
        if ([regexResult rangeAtIndex:2].location != NSNotFound) {
            NSString *sign = [string substringWithRange:[regexResult rangeAtIndex:2]];
            // hours
            seconds += [[NSString stringWithFormat:@"%@%@", sign, [string substringWithRange:[regexResult rangeAtIndex:3]]] doubleValue] * 60.0 * 60.0;
            // minutes
            seconds += [[NSString stringWithFormat:@"%@%@", sign, [string substringWithRange:[regexResult rangeAtIndex:4]]] doubleValue] * 60.0;
        }
        
        return [NSDate dateWithTimeIntervalSince1970:seconds];
    }
    return nil;
}

+ (NSString*) stringFromNumber:(NSNumber*)number
{
    return [self stringFromNumber:number withGroupSize:3];
}

+ (NSString*) stringFromNumber:(NSNumber*)number withGroupSize:(NSInteger)group
{
    NSNumberFormatter *formatter = [[NSNumberFormatter alloc] init];
    formatter.numberStyle = NSNumberFormatterDecimalStyle;
    formatter.groupingSize = group;
    formatter.groupingSeparator = @",";
    NSString* formattedString = [formatter stringFromNumber:number];
    [formatter release];
    return formattedString;
    
}

+ (NSArray*) splitStringIntoTwoLines:(NSString*)string withFont:(UIFont*)font constrainedToWidth:(float)width
{   
    CGFloat rowH = font.lineHeight;
    CGSize size = [string sizeWithFont:font constrainedToSize:CGSizeMake(width, 1000) lineBreakMode:UILineBreakModeWordWrap];    
    NSArray *result;
    NSString *firstLine;
    NSString *secondLine;   
    int indexWhenLineRedused = -1;
    if (size.height > rowH) {        
        //get first and second line text
        firstLine = [string substringToIndex:string.length -1];
        secondLine = [string substringFromIndex:string.length -1];
        
        CGSize theSize = [firstLine sizeWithFont:font constrainedToSize:CGSizeMake(width, 1000) lineBreakMode:UILineBreakModeWordWrap]; 
        BOOL finihed = NO;
        while (!finihed) {
            if (indexWhenLineRedused < 0 && theSize.height <= rowH) {
                indexWhenLineRedused = firstLine.length;
            }
            if(!firstLine.length || (theSize.height <= rowH && [[firstLine substringFromIndex:firstLine.length -1] isEqualToString:@" "]) ) {
                finihed = YES;                
            }
            if (!finihed) {
                secondLine = [NSString stringWithFormat:@"%@%@",[firstLine substringFromIndex:firstLine.length -1], secondLine];
                firstLine = [firstLine substringToIndex:firstLine.length -1];            
                theSize = [firstLine sizeWithFont:font constrainedToSize:CGSizeMake(width, 200) lineBreakMode:UILineBreakModeWordWrap]; 
            }   
        }
        //DLog(@"12345: %@ : %@", firstLine, secondLine);
        if (!firstLine.length) {//cant perform word wrap - so just split word
            firstLine = [string substringToIndex:indexWhenLineRedused];
            secondLine = [string substringFromIndex:indexWhenLineRedused];
        }
        result = [NSArray arrayWithObjects:firstLine, secondLine, nil];
    } else {
        result = [NSArray arrayWithObject:string];
    }
    return result;
}

+ (NSString*) trimString:(NSString*) trimmedString
{
    return [trimmedString stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

+ (NSDate *) dateToLocalDate: (NSDate *) date {
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = [tz secondsFromGMTForDate:date];
    
    DLog(@"Local date %@", [NSDate dateWithTimeInterval:seconds sinceDate:date]);
    return [NSDate dateWithTimeInterval:seconds sinceDate:date];
}

+ (NSDate *) dateToGMTDate: (NSDate *) date {
    NSTimeZone *tz = [NSTimeZone defaultTimeZone];
    NSInteger seconds = -[tz secondsFromGMTForDate:date];
    return [NSDate dateWithTimeInterval:seconds sinceDate:date];
}

@end
