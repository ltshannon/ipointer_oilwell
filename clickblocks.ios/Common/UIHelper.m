//
//  UIHelper.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/7/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "UIHelper.h"

NSString* const kGillSansStdCondensed = @"GillSans-Condensed";
NSString* const kGillSansStdBoldCondensed = @"GillSans-CondensedBold";
NSString* const kGillSansStd = @"GillSansStd";
NSString* const kGillSansStdBold = @"GillSansStd-Bold";

@implementation UIHelper

+ (UIImage *) bigYellowButtonImage {
    return [[UIImage imageNamed:@"btn_yellow_big.png"] stretchableImageWithLeftCapWidth:24 topCapHeight:0];
}

+ (UIImage *) regularYellowButtonImage {
    return [[UIImage imageNamed:@"btn_yellow.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
}

+ (UIImage *) tabButtonImage {
    return [[UIImage imageNamed:@"btn_tab.png"] stretchableImageWithLeftCapWidth:14 topCapHeight:0];
}

+ (UIImage *) tabButtonSelectedImage {
    return [[UIImage imageNamed:@"btn_tab_selected.png"] stretchableImageWithLeftCapWidth:13 topCapHeight:0];
}

+ (UIImage *) loginTextFieldImage {
    return [[UIImage imageNamed:@"tf_login_bg.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
}

+ (UIImage *) loginTextFieldSelectedImage {
    return [[UIImage imageNamed:@"tf_login_a_bg.png"] stretchableImageWithLeftCapWidth:20 topCapHeight:0];
}

@end


UIColor *rgb(CGFloat r, CGFloat g, CGFloat b) {
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:1.0];
}

UIColor *rgba(CGFloat r, CGFloat g, CGFloat b, CGFloat alpha) {
    return [UIColor colorWithRed:r/255.0 green:g/255.0 blue:b/255.0 alpha:alpha];
}

