//
//  Constants.h
//  clickblocks.ios
//
//  Created by Saritasa on 9/27/12.
//  Copyright (c) 2012 Saritasa. All rights reserved.
//


#define kSessionTokenDataKey @"session_token"
#define kSessionTimeout @"session_timeout"
#define kSessionUserName @"session_username"
#define kSessionPassword @"session_password"
#define kRememberedEmailDataKey @"login_email"
#define kRememberedPasswordDataKey @"login_password"
#define kRememberedRememberFlagDataKey @"login_remember"

