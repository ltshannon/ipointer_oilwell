//
//  UIHelper.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/7/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <Foundation/Foundation.h>

extern NSString* const kGillSansStdCondensed;
extern NSString* const kGillSansStdBoldCondensed;
extern NSString* const kGillSansStd;
extern NSString* const kGillSansStdBold;

@interface UIHelper : NSObject

+ (UIImage *) bigYellowButtonImage;
+ (UIImage *) regularYellowButtonImage;
+ (UIImage *) tabButtonImage;
+ (UIImage *) tabButtonSelectedImage;
+ (UIImage *) loginTextFieldImage;
+ (UIImage *) loginTextFieldSelectedImage;

@end

UIColor *rgb(CGFloat r, CGFloat g, CGFloat b);
UIColor *rgba(CGFloat r, CGFloat g, CGFloat b, CGFloat alpha);
