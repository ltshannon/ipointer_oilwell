//
//  MapTabViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//


#import "AbstractTabViewController.h"
#import "CurrentLocationAnnotation.h"
#import "OilWellAnnotation.h"
#import "ArrowAnnotation.h"


@interface MapTabViewController : AbstractTabViewController <MKMapViewDelegate>

@property (retain, nonatomic) IBOutlet MKMapView *mapView;
@property (retain, nonatomic) CurrentLocationAnnotation *currentLocationAnnotation;
@property (retain, nonatomic) OilWellAnnotation *oilWellAnnotation;
@property (retain, nonatomic) ArrowAnnotation *arrowAnnotation;

- (void) updateAnnotations;

@end
