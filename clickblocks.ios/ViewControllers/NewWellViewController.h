//
//  NewWellViewController.h
//  OilWell
//
//  Created by LAWRENCE SHANNON on 9/28/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "AbstractTabViewController.h"

@interface NewWellViewController : AbstractTabViewController

@property (retain, nonatomic) IBOutlet UIScrollView *myScrollView;
@property (retain, nonatomic) IBOutlet UITextView *myTextView;


@end
