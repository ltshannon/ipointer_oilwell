//
//  MapTabViewController.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "MapTabViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "coreLocationController.h"
#import "CurrentLocationAnnotationView.h"
#import "OilWellAnnotationView.h"


@implementation MapTabViewController

#pragma mark - Lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	
    _mapView.layer.cornerRadius = 5;
    
    [self updateAnnotations];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc
{
    [_currentLocationAnnotation release];
    [_oilWellAnnotation release];
    [_arrowAnnotation release];
    [_mapView release];
    [super dealloc];
}

- (void)viewDidUnload
{
    [self setMapView:nil];
    [super viewDidUnload];
}

- (void) updateAnnotations {
    if ([CoreLocationController sharedInstance].locationAvailable) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[[CoreLocationController sharedInstance] latitude] doubleValue],
                                                                       [[[CoreLocationController sharedInstance] longitude] doubleValue]);
        self.currentLocationAnnotation= [[[CurrentLocationAnnotation alloc] initWithLocation:coordinate] autorelease];
        [self.mapView addAnnotation:_currentLocationAnnotation];
        
        if (!self.wellData) {
            self.arrowAnnotation = [[[ArrowAnnotation alloc] initWithLocation:coordinate] autorelease];
            _arrowAnnotation.heading = [CoreLocationController sharedInstance].heading;
            [self.mapView addAnnotation:_arrowAnnotation];
        }
    }
    
    if (self.wellData) {
        CLLocationCoordinate2D coordinate = CLLocationCoordinate2DMake([[[self.wellData objectForKey:@"well"] objectForKey:@"Latitude"] doubleValue],
                                                                       [[[self.wellData objectForKey:@"well"] objectForKey:@"Longitude"] doubleValue]);
        
        self.oilWellAnnotation = [[[OilWellAnnotation alloc] initWithLocation:coordinate wellData:self.wellData] autorelease];
        [self.mapView addAnnotation:_oilWellAnnotation];
    }
    
    MKMapRect zoomRect = MKMapRectNull;
    for (id <MKAnnotation> annotation in _mapView.annotations)
    {
        MKMapPoint annotationPoint = MKMapPointForCoordinate(annotation.coordinate);
//        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 0.1);
        MKMapRect pointRect = MKMapRectMake(annotationPoint.x, annotationPoint.y, 0.1, 500);
        if (MKMapRectIsNull(zoomRect)) {
            zoomRect = pointRect;
        } else {
            zoomRect = MKMapRectUnion(zoomRect, pointRect);
        }
    }
    
    int topPadding = 50;
    int bottomPadding = 50;
    DLog(@"[_mapView.annotations count] = %d", [_mapView.annotations count]);
//    if ([_mapView.annotations count] == 2) {
//        topPadding = _mapView.height / 2;
//        bottomPadding = 0;
//    }
    
    [_mapView setVisibleMapRect:zoomRect edgePadding:UIEdgeInsetsMake(topPadding, 100, bottomPadding, 100) animated:YES];
}


#pragma mark - MKMapViewDelegate

- (MKAnnotationView *) mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation
{
    if ( [annotation isKindOfClass:[MKUserLocation class]] )
    {
        return nil;
    }
    
    if ( [annotation isKindOfClass:[CurrentLocationAnnotation class]] )
    {
        CurrentLocationAnnotationView* pinView = (CurrentLocationAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"CurrentLocationAnnotationView"];
        
        if ( !pinView )
        {
            pinView = [[[CurrentLocationAnnotationView alloc]
                        initWithAnnotation:annotation
                        reuseIdentifier:@"CurrentLocationAnnotationView"] autorelease];
            pinView.canShowCallout = YES;
        }
        else
        {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    else if ( [annotation isKindOfClass:[OilWellAnnotation class]] )
    {
        OilWellAnnotationView* pinView = (OilWellAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"OilWellAnnotationView"];
        
        if ( !pinView )
        {
            pinView = [[[OilWellAnnotationView alloc]
                        initWithAnnotation:annotation
                        reuseIdentifier:@"OilWellAnnotationView"] autorelease];
            pinView.canShowCallout = YES;
        }
        else
        {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    else if ( [annotation isKindOfClass:[ArrowAnnotation class]] )
    {
        MKAnnotationView* pinView = [mapView dequeueReusableAnnotationViewWithIdentifier:@"ArrowAnnotation"];
        
        if ( !pinView )
        {
            pinView = [[[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"ArrowAnnotation"] autorelease];
            pinView.canShowCallout = YES;
            [pinView setImage:[UIImage imageNamed:@"arrow_red_modified.png"]];
            pinView.transform = CGAffineTransformMakeRotation(_arrowAnnotation.heading * M_PI/180.0);
        }
        else
        {
            pinView.annotation = annotation;
        }
        
        return pinView;
    }
    
    return nil;
}

//- (void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated {
//    DLog(@"zoomLevel = %d", [mapView zoomLevel]);
//    
//    if( [mapView zoomLevel] > 19 )
//    {
//        DLog(@"");
//        [mapView setCenterCoordinate:mapView.centerCoordinate zoomLevel:19 animated:NO];
//    }
//}

//- (void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated {
//    DLog(@"zoomLevel = %d", [mapView zoomLevel]);
//    
//    if( [mapView zoomLevel] > 19 )
//    {
//        DLog(@"");
//        [mapView setCenterCoordinate:mapView.centerCoordinate zoomLevel:19 animated:NO];
//    }
//}

@end
