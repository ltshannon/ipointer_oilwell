//
//  AuditTabViewController.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "AuditTabViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIHelper.h"
#import "Common.h"
#import "ViewController.h"
#import "APIClient.h"
#import "DSActivityView.h"
#import "Constants.h"

#define kCheckbox1TagOffset 100
#define kCheckbox2TagOffset 200
#define kTextViewTagOffset 300
#define kCellTextViewWidth 745
#define kCellNameWidth 745

@interface AuditTabViewController ()

//@property (retain, nonatomic) NSMutableIndexSet *checkedIndexSet;
@property (retain, nonatomic) UITextView *editedTextView;

@end

@implementation AuditTabViewController

NSMutableArray *t;
NSMutableDictionary *p;

- (void)dealloc {
    [_chemicalButton release];
    [_tableView release];
//    [_checkedIndexSet release];
    [_textView release];
    [_placeholderLabel release];
    [_chemicals release];
    [_chemicalPopoverController release];
    [_editedTextView release];
    [_dateLabel release];
    [_nameLabel release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setChemicalButton:nil];
    [self setTableView:nil];
    [self setTextView:nil];
    [self setPlaceholderLabel:nil];
    [self setDateLabel:nil];
    [self setNameLabel:nil];
    [super viewDidUnload];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _chemicalButton.layer.cornerRadius = 10;
    _chemicalButton.layer.borderWidth = 2;
    _chemicalButton.layer.borderColor = [rgb(203, 203, 203) CGColor];
    
    _textView.layer.cornerRadius = 10;
    _textView.layer.borderWidth = 2;
    _textView.layer.borderColor = [rgb(203, 203, 203) CGColor];
    _textView.hidden = YES;
    _placeholderLabel.hidden = YES;
    
//    CAGradientLayer *gradient = [CAGradientLayer layer];
//    gradient.frame = CGRectMake(_tableView.left, _tableView.top, _tableView.width, 10);
//    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor colorWithWhite:1 alpha:0] CGColor], nil];
//    [self.view.layer addSublayer:gradient];
//    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(_tableView.left, _tableView.bottom - 10, _tableView.width, 10);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:1 alpha:0] CGColor], (id)[[UIColor whiteColor] CGColor], nil];
    [self.view.layer addSublayer:gradient];
    
//    NSString *comments = [self.wellData objectForKey:@"COMMENTS"];
//    _textView.text = [comments isKindOfClass:[NSString class]] ? [comments trim] : nil;
//    _placeholderLabel.hidden = [_textView.text length] > 0;
    
//    self.checkedIndexSet = [NSMutableIndexSet indexSet];
    DLog(@"%@", self.wellData);
    
    self.chemicals = [NSArray arrayWithObjects:@"Default", @"PAW4HF", @"WCW4527", @"CRW9257", nil];
    _selectedChemicalIndex = 0;
    [_chemicalButton setTitle:[_chemicals objectAtIndex:_selectedChemicalIndex] forState:UIControlStateNormal];
 
    [self loadAudit];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [super viewDidDisappear:animated];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) enableSaveButton: (BOOL) enable {
    if (self.delegate && [self.delegate respondsToSelector:@selector(tabController:performedAction:)]) {
        [self.delegate tabController:self performedAction:[NSDictionary dictionaryWithObjectsAndKeys:@"enableSaveButton", @"action", [NSNumber numberWithBool:enable], @"enable", nil]];
    }
}

- (void) loadAudit
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    NSString *chemichalType = [_chemicals objectAtIndex:_selectedChemicalIndex];
    if (chemichalType) {
        [params setObject:chemichalType forKey:@"chemicalType"];
    }
    
// LTS 9/23/2013
    [params setObject:[[self.wellData objectForKey:@"well"] objectForKey:@"APINumber"] forKey:@"wellAPI"];
//    [params setObject:@"2976966" forKey:@"wellAPI"];

    [DSBezelActivityView newActivityViewForView:self.view];
    [[APIClient sharedInstance] callApi:kApiKeyAudit httpMethod:kHTTPMethodGET parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [DSBezelActivityView removeViewAnimated:YES];
        
        self.properties = [NSMutableArray array];//[responseObject objectForKey:@"result"];
//        [_checkedIndexSet removeAllIndexes];
        
        for (NSDictionary *d in [responseObject objectForKey:@"result"]) {
            [_properties addObject:[NSMutableDictionary dictionaryWithDictionary:d]];
        }
        
        if ([_properties count] > 0)
        {
            NSDictionary *dict = [_properties objectAtIndex:1];

            NSString *dateString = [dict objectForKey:@"lastDateTime"];
            if (![dateString isKindOfClass:[NSString class]])
            {
                self.dateLabel.text = @"n/a";
            }
            else
            {
                NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
                [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
                NSDate *date = [formatter dateFromString:dateString];
                
                // LTS 9-25-2013
                //        [formatter setDateFormat:@"MMM dd, yyyy h:mm a"];
                [formatter setDateFormat:@"MM/dd/yy"];
                dateString = [formatter stringFromDate:date];
                [formatter release];
                
                self.dateLabel.text = dateString;
            }
            
            NSString *authorString = [dict objectForKey:@"lastBy"];
            if (![authorString isKindOfClass:[NSString class]])
            {
                self.nameLabel.text = @"n/a";
            }
            else
            {
                self.nameLabel.text = authorString;
            }
        }
        
        [_tableView reloadData];
        [self enableSaveButton:YES];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [DSBezelActivityView removeViewAnimated:YES];
        
        NSString *errorDesc = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorDesc delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }];
}

- (void) saveAudit
{
//    NSString *comments = [_textView.text trim];
    
    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    [params setObject:[[self.wellData objectForKey:@"well"] objectForKey:@"APINumber"] forKey:@"wellAPI"];
//    [params setObject:(comments ? comments : @"") forKey:@"comments"];
    
    
    __block NSString *message1 = nil;
    __block NSString *message2 = nil;
    NSMutableArray *propertyIDs = [NSMutableArray array];
    
    [_properties enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSDictionary *dict = (NSDictionary *)obj;
        
        NSMutableDictionary *pdict = [NSMutableDictionary dictionary];
        if ([dict objectForKey:@"propertyID"])
            [pdict setObject:[dict objectForKey:@"propertyID"] forKey:@"ID"];
        
        id success = [dict objectForKey:@"lastSuccess"];
        if (!success || [success isKindOfClass:[NSNull class]]) {
            message1 = @"You must select OK or NO for each row.";
        } else {
            [pdict setObject:success forKey:@"success"];
            
            if ([success boolValue] == NO) {
                NSString *comment = [dict objectForKey:@"lastComment"];
                if ([comment isKindOfClass:[NSNull class]] || ![comment length]) {
                    message2 = @"You must comment all rows with selected NO.";
                } else {
                    [pdict setObject:comment forKey:@"comment"];
                }
            }
        }
        
        [propertyIDs addObject:pdict];
    }];
    
    [params setObject:propertyIDs forKey:@"properties"];
    
    
    if (message1 || message2) {
        NSString *message = nil;
        if (message1) message = message1;
        if (message2) {
            if (message) {
                message = [message stringByAppendingFormat:@"\n%@", message2];
            } else {
                message = message2;
            }
        }
        
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:message message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
        
        return;
    }
    
    
    [self enableSaveButton:NO];
    [DSBezelActivityView newActivityViewForView:self.view];
    [[APIClient sharedInstance] callApi:kApiKeySaveAudit httpMethod:kHTTPMethodGET parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [DSBezelActivityView removeViewAnimated:NO];
        [self loadAudit];
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [DSBezelActivityView removeViewAnimated:YES];
        [self enableSaveButton:YES];
        
        NSString *errorDesc = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorDesc delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }];
}

- (IBAction)clickChemical:(id)sender
{
    ChemicalsPopoverContentViewController *controller = [[ChemicalsPopoverContentViewController alloc] initWithStyle:UITableViewStylePlain];
    controller.delegate = self;
    controller.chemicals = _chemicals;
    controller.selectedChemicalIndex = _selectedChemicalIndex;
    UINavigationController *navController = [[UINavigationController alloc] initWithRootViewController:controller];
    self.chemicalPopoverController = [[[UIPopoverController alloc] initWithContentViewController:navController] autorelease];
    _chemicalPopoverController.delegate = self;
    [navController release];
    controller.popover = _chemicalPopoverController;
    [controller release];
    _chemicalPopoverController.popoverContentSize = CGSizeMake(320, 260);
    [_chemicalPopoverController presentPopoverFromRect:_chemicalButton.frame inView:self.view permittedArrowDirections:UIPopoverArrowDirectionUp animated:YES];
}

- (void) clickCheckbox1: (UIButton *) sender
{
    sender.selected = !sender.selected;
    
    NSInteger index = sender.tag - kCheckbox1TagOffset;
//    if (sender.selected) {
//        [_checkedIndexSet addIndex:index];
//    } else {
//        [_checkedIndexSet removeIndex:index];
//    }
    
    
    UIButton *btn1 = sender;
    UIButton *btn2 = nil;
    for (UIView *v in sender.superview.subviews)
    {
        if ([v isKindOfClass:[UIButton class]])
        {
            if (v != sender)
            {
                btn2 = (UIButton *)v;
                break;
            }
        }
    }
    
    BOOL prevBtn2Selected = btn2.selected;
    
    if (btn1.selected) btn2.selected = NO;
    
    NSMutableDictionary *dict = [_properties objectAtIndex:index];
    [dict setObject:(btn1.selected ? @"1" : [NSNull null]) forKey:@"lastSuccess"];
    
    if (prevBtn2Selected != btn2.selected) {
        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

- (void) clickCheckbox2: (UIButton *) sender
{
    sender.selected = !sender.selected;
    
    NSInteger index = sender.tag - kCheckbox2TagOffset;
//    if (sender.selected) {
//        [_checkedIndexSet addIndex:index];
//    } else {
//        [_checkedIndexSet removeIndex:index];
//    }
    
    UIButton *btn1 = nil;
    UIButton *btn2 = sender;
    for (UIView *v in sender.superview.subviews)
    {
        if ([v isKindOfClass:[UIButton class]])
        {
            if (v != sender)
            {
                btn1 = (UIButton *)v;
                break;
            }
        }
    }
    
    BOOL prevBtn2Selected = !btn2.selected;
    
    if (btn2.selected)
    {
        btn1.selected = NO;
    }
    NSMutableDictionary *dict = [_properties objectAtIndex:index];
    [dict setObject:(btn2.selected ? @"0" : [NSNull null]) forKey:@"lastSuccess"];
    
    if (prevBtn2Selected != btn2.selected)
    {
        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }
}

#pragma mark - UITableView

- (NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [_properties count];
}

- (CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSDictionary *dict = [_properties objectAtIndex:indexPath.row];
    NSLog(@"row : %i", indexPath.row);
    CGFloat addHeight = 0;
    if (!([[dict objectForKey:@"lastSuccess"] isKindOfClass:[NSNull class]] || [[dict objectForKey:@"lastSuccess"] boolValue]))
    {
        addHeight += 40;
        NSString *comment = [dict objectForKey:@"lastComment"];
        if (![comment isKindOfClass:[NSNull class]])
        {
            CGSize size = [comment sizeWithFont:[UIFont systemFontOfSize:14] constrainedToSize:CGSizeMake(kCellTextViewWidth, 10000)];
            if (size.height > 35) {
                addHeight = size.height + 15;
            }
        }
    }
    if (![[dict objectForKey:@"name"] isKindOfClass:[NSNull class]])
    {
        NSString *name = [dict objectForKey:@"name"];
        if (![name isKindOfClass:[NSNull class]])
        {
            CGSize size = [name sizeWithFont:[UIFont fontWithName:kGillSansStdBold size:17] constrainedToSize:CGSizeMake(kCellNameWidth, 10000)];
            if (size.height > 17) {
                addHeight += (size.height / 17) * 17;
            }
        }
    }

    return 44 + addHeight;
}

- (UITableViewCell *) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *cellIdentifier = @"Cell";

    NSDictionary *dict = [_properties objectAtIndex:indexPath.row];

// LTS 9-26-2013
//    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:cellIdentifier];

//    if (cell == nil)
//    {
//        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        UITableViewCell *cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:cellIdentifier];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        
        UIButton *btn = [[UIButton alloc] initWithFrame:CGRectMake(10, 7, 32, 32)];
        [btn setBackgroundImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"check_mark.png"] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(clickCheckbox1:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btn];
        [btn release];
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(45, 12, 30, 25)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = rgb(42, 42, 42);
        label.font = [UIFont fontWithName:kGillSansStdBold size:14];
        label.text = @"OK";
        [cell.contentView addSubview:label];
        [label release];
        
        btn = [[UIButton alloc] initWithFrame:CGRectMake(80, 7, 32, 32)];
        [btn setBackgroundImage:[UIImage imageNamed:@"checkbox.png"] forState:UIControlStateNormal];
        [btn setImage:[UIImage imageNamed:@"check_mark.png"] forState:UIControlStateSelected];
        [btn addTarget:self action:@selector(clickCheckbox2:) forControlEvents:UIControlEventTouchUpInside];
        [cell.contentView addSubview:btn];
        [btn release];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(115, 12, 30, 25)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = rgb(42, 42, 42);
        label.font = [UIFont fontWithName:kGillSansStdBold size:14];
        label.text = @"NO";
        [cell.contentView addSubview:label];
        [label release];
        
// LTS 9-25-2013
        // question
        NSString *name = [dict objectForKey:@"name"];
        label = [[UILabel alloc] init];
        CGFloat height = 25;
        if (![name isKindOfClass:[NSNull class]]) {
            CGSize size = [name sizeWithFont:[UIFont fontWithName:kGillSansStdBold size:17] constrainedToSize:CGSizeMake(kCellNameWidth, 10000)];
            if (size.height >= 34)
            {
                int lines = (size.height / 17);
                label.numberOfLines = lines;
                height = 25 * lines;
            }
        }

        label.frame= CGRectMake(160, 12, kCellNameWidth, height);
        label.backgroundColor = [UIColor clearColor];
        label.textColor = rgb(42, 42, 42);
        label.font = [UIFont fontWithName:kGillSansStdBold size:17];
        label.tag = 1;
        label.text = [[dict objectForKey:@"name"] description];
        label.lineBreakMode = NSLineBreakByWordWrapping;
        label.adjustsFontSizeToFitWidth = NO;
        [cell.contentView addSubview:label];
        [label release];

// LTS 9-25-2013
        // date
/*
        label = [[UILabel alloc] initWithFrame:CGRectMake(728, 12, 75, 25)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = rgb(42, 42, 42);
        label.font = [UIFont fontWithName:kGillSansStd size:17];
        label.tag = 2;
        [cell.contentView addSubview:label];
        [label release];

// LTS 9-25-2013
        // author
        label = [[UILabel alloc] initWithFrame:CGRectMake(803, 12, 100, 25)];
        label.backgroundColor = [UIColor clearColor];
        label.textColor = rgb(42, 42, 42);
        label.font = [UIFont fontWithName:kGillSansStd size:17];
        label.tag = 3;
        [cell.contentView addSubview:label];
        [label release];
*/

        UITextView *tv = [[UITextView alloc] initWithFrame:CGRectMake(160, 20 + height, kCellTextViewWidth, 35)];
        tv.layer.cornerRadius = 10;
        tv.layer.borderWidth = 2;
        tv.layer.borderColor = [rgb(203, 203, 203) CGColor];
        tv.font = [UIFont systemFontOfSize:14];
        tv.delegate = self;
        tv.scrollEnabled = NO;

        [cell.contentView addSubview:tv];
        [tv release];
//    }
    
    cell.contentView.backgroundColor = indexPath.row%2 ? [UIColor clearColor] : rgb(234, 234, 234);
    
    UIButton *btn1 = nil;
    UIButton *btn2 = nil;
    for (UIView *v in cell.contentView.subviews)
    {
        if ([v isKindOfClass:[UIButton class]])
        {
            if (!btn1) {
                btn1 = (UIButton *)v;
            } else
            {
                btn2 = (UIButton *)v;
                break;
            }
        }
    }
    
    btn1.tag = kCheckbox1TagOffset + indexPath.row;
    btn2.tag = kCheckbox2TagOffset + indexPath.row;
//    btn.selected = [_checkedIndexSet containsIndex:indexPath.row];
    

    if ([[dict objectForKey:@"lastSuccess"] isKindOfClass:[NSNull class]])
    {
        btn1.selected = NO;
        btn2.selected = NO;
    } else
    {
        btn1.selected = [[dict objectForKey:@"lastSuccess"] boolValue] == YES;
        btn2.selected = [[dict objectForKey:@"lastSuccess"] boolValue] == NO;
    }
    
//    UILabel *label = (UILabel *)[cell.contentView viewWithTag:1];
//    label.text = [[dict objectForKey:@"name"] description];
/*
    label = (UILabel *)[cell.contentView viewWithTag:2];
    NSString *dateString = [dict objectForKey:@"lastDateTime"];
    if (![dateString isKindOfClass:[NSString class]]) {
        label.text = @"n/a";
    } else {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
        NSDate *date = [formatter dateFromString:dateString];

// LTS 9-25-2013
//        [formatter setDateFormat:@"MMM dd, yyyy h:mm a"];
        [formatter setDateFormat:@"MM/dd/yy"];
        dateString = [formatter stringFromDate:date];
        [formatter release];
        
        label.text = dateString;
    }
    
    label = (UILabel *)[cell.contentView viewWithTag:3];
    NSString *authorString = [dict objectForKey:@"lastBy"];
    if (![authorString isKindOfClass:[NSString class]]) {
        label.text = @"n/a";
    } else {
        label.text = authorString;
    }
*/
    tv = nil;//(UITextView*)[cell.contentView viewWithTag:4];
    
    for (UIView *v in cell.contentView.subviews)
    {
        if ([v isKindOfClass:[UITextView class]])
        {
            tv = (UITextView *)v;
            break;
        }
    }
    
    tv.tag = kTextViewTagOffset + indexPath.row;
    tv.text = nil;
    tv.hidden = YES;
    if (btn2.selected)
    {
        CGFloat height = 35;
        NSString *comment = [dict objectForKey:@"lastComment"];
        if (![comment isKindOfClass:[NSNull class]])
        {
            CGSize size = [comment sizeWithFont:tv.font constrainedToSize:CGSizeMake(kCellTextViewWidth, 10000)];
            if (size.height > 35)
            {
                height = size.height + 15;
            }
            
            tv.text = [dict objectForKey:@"lastComment"];
        }
        
        tv.height = height;
        tv.hidden = NO;
    }
    
    return cell;
}

- (void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}


#pragma mark - KeyboardDetect


- (void)keyboardWillHide: (NSNotification *)notif
{
    CGFloat duration = [[notif.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:duration animations:^{
//        _textView.height = 35;
        _textView.top = 330;
        self.dateLabel.hidden = NO;
        self.nameLabel.hidden = NO;
    } completion:^(BOOL finished) {
//        _placeholderLabel.hidden = [_textView.text length] > 0;
        _textView.hidden = YES;
        
        NSInteger index = self.editedTextView.tag - kTextViewTagOffset;
        
        self.editedTextView.text = [_textView.text trim];
        if (self.editedTextView.text) {
            NSMutableDictionary *dict = [_properties objectAtIndex:index];
            [dict setObject:self.editedTextView.text forKey:@"lastComment"];
        }
        [_tableView reloadRowsAtIndexPaths:[NSArray arrayWithObject:[NSIndexPath indexPathForRow:index inSection:0]] withRowAnimation:UITableViewRowAnimationFade];
    }];
}


- (void)keyboardWillShow: (NSNotification *)notif
{
    CGFloat duration = [[notif.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    CGRect kbRect = [[notif.userInfo objectForKey:UIKeyboardFrameBeginUserInfoKey] CGRectValue];
    CGFloat tvHeight = [Common rootController].view.height - self.view.superview.top - kbRect.size.width - 9;
    
    _textView.text = self.editedTextView.text;
    self.dateLabel.hidden = YES;
    self.nameLabel.hidden = YES;
    
//    _placeholderLabel.hidden = YES;
    _textView.hidden = NO;
    [UIView animateWithDuration:duration animations:^{
        _textView.height = tvHeight;
        _textView.top = 9;
    }];
}

#pragma mark - UITextViewDelegate

- (BOOL) textViewShouldBeginEditing:(UITextView *)textView
{
    if (textView != _textView) {
        self.editedTextView = textView;
        [_textView becomeFirstResponder];
        return NO;
    }
    
    return YES;
}


#pragma mark - UIPopoverControllerDelegate

- (void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    if (popoverController == _chemicalPopoverController) {
        self.chemicalPopoverController = nil;
    }
}


#pragma mark - ChemicalsPopoverContentViewControllerDelegate

- (void) chemicalsPopoverContentViewController:(ChemicalsPopoverContentViewController *)controller didSelectIndex:(NSInteger)index
{
    _selectedChemicalIndex = index;
    [_chemicalButton setTitle:[_chemicals objectAtIndex:_selectedChemicalIndex] forState:UIControlStateNormal];
    [_chemicalPopoverController dismissPopoverAnimated:YES];
    
    [self loadAudit];
}

@end
