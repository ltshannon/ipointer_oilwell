//
//  ChemicalsPopoverContentViewController.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/14/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "ChemicalsPopoverContentViewController.h"

@interface ChemicalsPopoverContentViewController ()

@end

@implementation ChemicalsPopoverContentViewController

- (void)dealloc
{
    DLog(@"");
    [_chemicals release];
    [super dealloc];
}

- (id)initWithStyle:(UITableViewStyle)style
{
    self = [super initWithStyle:style];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];

    self.title = @"Chemicals";
    self.navigationItem.leftBarButtonItem = [[[UIBarButtonItem alloc] initWithTitle:@"Close" style:UIBarButtonItemStyleBordered target:self action:@selector(clickClose:)] autorelease];
}

- (void) clickClose: (id) sender {
    [_popover dismissPopoverAnimated:YES];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    // Return the number of sections.
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    // Return the number of rows in the section.
    return [_chemicals count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier] autorelease];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    
    // Configure the cell...
    
    cell.textLabel.text = [_chemicals objectAtIndex:indexPath.row];
    cell.accessoryType = indexPath.row == _selectedChemicalIndex ? UITableViewCellAccessoryCheckmark : UITableViewCellAccessoryNone;
    
    return cell;
}

#pragma mark - Table view delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    _selectedChemicalIndex = indexPath.row;
    [self.tableView reloadData];
    
    if (_delegate && [_delegate respondsToSelector:@selector(chemicalsPopoverContentViewController:didSelectIndex:)]) {
        [_delegate chemicalsPopoverContentViewController:self didSelectIndex:_selectedChemicalIndex];
    }
}

@end
