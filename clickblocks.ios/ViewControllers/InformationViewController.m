//
//  InformationViewController.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "InformationViewController.h"
#import "GeneralTabViewController.h"
#import "MapTabViewController.h"
#import "AuditTabViewController.h"
#import "SearchTabViewController.h"
#import "NewWellViewController.h"
#import <QuartzCore/QuartzCore.h>
#import "UIHelper.h"
#import "APIClient.h"
#import "Constants.h"
#import "DSActivityView.h"
#import "coreLocationController.h"

@interface InformationViewController ()

@end

@implementation InformationViewController

- (void)dealloc
{
    [_tabGeneralButton release];
    [_tabMapButton release];
    [_tabAuditButton release];
    [_tabSearchButton release];
    [_tabNewWellButton release];
    [_pointButton release];
    [_contentView release];
    [_generalController release];
    [_mapController release];
    [_auditController release];
    [_searchController release];
    [_currentController release];
    [_cornerFixView release];
    [_submitButton release];
    [_titleLabel release];
    
    [_wellData release];
    
    [_debugLinkButton release];
    [_distanceLabel release];
    [super dealloc];
}

- (void)viewDidUnload
{
    [self setTabGeneralButton:nil];
    [self setTabMapButton:nil];
    [self setTabAuditButton:nil];
    [self setTabSearchButton:nil];
    [self setPointButton:nil];
    [self setContentView:nil];
    [self setCornerFixView:nil];
    [self setSubmitButton:nil];
    [self setTitleLabel:nil];
    [self setDebugLinkButton:nil];
    [self setDistanceLabel:nil];
    [self setTabNewWellButton:nil];
    [super viewDidUnload];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    _contentView.clipsToBounds = YES;
    _contentView.layer.cornerRadius = 10;
    
    [_tabGeneralButton setBackgroundImage:[UIHelper tabButtonImage] forState:UIControlStateNormal];
    [_tabGeneralButton setBackgroundImage:[UIHelper tabButtonSelectedImage] forState:UIControlStateSelected];
    [_tabMapButton setBackgroundImage:[UIHelper tabButtonImage] forState:UIControlStateNormal];
    [_tabMapButton setBackgroundImage:[UIHelper tabButtonSelectedImage] forState:UIControlStateSelected];
    [_tabAuditButton setBackgroundImage:[UIHelper tabButtonImage] forState:UIControlStateNormal];
    [_tabAuditButton setBackgroundImage:[UIHelper tabButtonSelectedImage] forState:UIControlStateSelected];
    [_tabSearchButton setBackgroundImage:[UIHelper tabButtonImage] forState:UIControlStateNormal];
    [_tabSearchButton setBackgroundImage:[UIHelper tabButtonSelectedImage] forState:UIControlStateSelected];
    [_tabNewWellButton setBackgroundImage:[UIHelper tabButtonImage] forState:UIControlStateNormal];
    [_tabNewWellButton setBackgroundImage:[UIHelper tabButtonSelectedImage] forState:UIControlStateSelected];

    
    [_submitButton setBackgroundImage:[UIHelper regularYellowButtonImage] forState:UIControlStateNormal];
    _submitButton.titleLabel.font = [UIFont fontWithName:kGillSansStdBoldCondensed size:_submitButton.titleLabel.font.pointSize];
    
    if (!_wellData) {
        [self clickTabButton:_tabMapButton];
        _tabGeneralButton.enabled = _tabAuditButton.enabled = NO;
    } else {
        [self clickTabButton:_tabGeneralButton];
    }
    
    [self setTitleText];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void) setWellData:(NSDictionary *)wellData
{
    [_wellData release];
    _wellData = [wellData retain];
    
    [self setTitleText];
}

- (void) setTitleText
{
    _titleLabel.text = nil;
    _distanceLabel.text = nil;
    
    if (_wellData) {
        _titleLabel.text = [NSString stringWithFormat:@"%@ %@", [[[_wellData objectForKey:@"well"] objectForKey:@"OperatorNa"] description], [[[_wellData objectForKey:@"well"] objectForKey:@"APINumber"] description]];
        
        if ([_wellData objectForKey:@"distance"]) {
            NSInteger distanceFeet = [[[_wellData objectForKey:@"distance"] description] intValue] * 3.2808;
            
            NSString *azimuth = @"";
            int azimuthValue = [[_wellData objectForKey:@"azimuth"] intValue];
            if (azimuthValue > 45 && azimuthValue < 135)
            {
                azimuth = @"east";
            }
            else if (azimuthValue >= 135 && azimuthValue <= 225)
            {
                azimuth = @"south";
            }
            else if (azimuthValue > 225 && azimuthValue < 315)
            {
                azimuth = @"west";
            }
            else
            {
                azimuth = @"north";
            }
            
            _distanceLabel.text = [NSString stringWithFormat:@"This well is %dft. %@ of you", distanceFeet, azimuth];
        }
    }
}

- (IBAction)clickTabButton:(UIButton *)sender
{
    
    _tabGeneralButton.selected = _tabMapButton.selected = _tabAuditButton.selected = _tabSearchButton.selected = _tabNewWellButton.selected = NO;
    
    _tabGeneralButton.titleLabel.shadowOffset = CGSizeMake(0, 1);
    _tabMapButton.titleLabel.shadowOffset = _tabGeneralButton.titleLabel.shadowOffset;
    _tabAuditButton.titleLabel.shadowOffset = _tabGeneralButton.titleLabel.shadowOffset;
    _tabSearchButton.titleLabel.shadowOffset = _tabGeneralButton.titleLabel.shadowOffset;
    _tabNewWellButton.titleLabel.shadowOffset = _tabGeneralButton.titleLabel.shadowOffset;

    sender.selected = YES;
    sender.titleLabel.shadowOffset = CGSizeZero;
    
    _cornerFixView.hidden = !_tabGeneralButton.selected;
    _submitButton.hidden = !_tabAuditButton.selected;
    
    if (sender == _tabGeneralButton)
    {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            _generalController = [[GeneralTabViewController alloc] initWithNibName:@"GeneralTabViewController" bundle:nil];
        }
        else
        {
            _generalController = [[GeneralTabViewController alloc] initWithNibName:@"GeneralTabViewController" bundle:nil];
        }
        _generalController.wellData = _wellData;
        _generalController.delegate = self;
        self.currentController = _generalController;
    } else if (sender == _tabMapButton)
    {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            _mapController = [[MapTabViewController alloc] initWithNibName:@"MapTabViewController" bundle:nil];
        }
        else
        {
            _mapController = [[MapTabViewController alloc] initWithNibName:@"MapTabViewController_iPhone" bundle:nil];
        }
        _mapController.wellData = _wellData;
        _mapController.delegate = self;
        self.currentController = _mapController;

    } else if (sender == _tabAuditButton)
    {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            _auditController = [[AuditTabViewController alloc] initWithNibName:@"AuditTabViewController" bundle:nil];
        }
        else
        {
            _auditController = [[AuditTabViewController alloc] initWithNibName:@"AuditTabViewController" bundle:nil];
        }
        _auditController.wellData = _wellData;
        _auditController.delegate = self;
        self.currentController = _auditController;
    } else if (sender == _tabSearchButton)
    {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            _searchController = [[SearchTabViewController alloc] initWithNibName:@"SearchTabViewController" bundle:nil];
        }
        else
        {
            _searchController = [[SearchTabViewController alloc] initWithNibName:@"SearchTabViewController_iPhone" bundle:nil];
        }
        _searchController.wellData = _wellData;
        _searchController.delegate = self;
        self.currentController = _searchController;
    }
    else if (sender == _tabNewWellButton)
    {
        if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
        {
            _newWellController = [[NewWellViewController alloc] initWithNibName:@"NewWellViewController" bundle:nil];
        }
        else
        {
            _newWellController = [[NewWellViewController alloc] initWithNibName:@"NewWellViewController" bundle:nil];
        }
        _newWellController.wellData = _wellData;
        _newWellController.delegate = self;
        self.currentController = _newWellController;
    }

    [_contentView.subviews makeObjectsPerformSelector:@selector(removeFromSuperview)];
    
    [_currentController viewWillAppear:NO];
    [_contentView addSubview:_currentController.view];
    [_currentController viewDidAppear:NO];
}

- (IBAction)clickPoint:(id)sender
{
    NSMutableDictionary *params = [NSMutableDictionary dictionary];

// LTS 9/22/2013 Hardcode a data point.
/*
// APINumber 2976966
    [params setObject:@"35.313269" forKey:@"latitude"];
    [params setObject:@"-119.064432" forKey:@"longitude"];

// APINumber 2900346
//    [params setObject:@"35.341976" forKey:@"latitude"];
//    [params setObject:@"-118.908783" forKey:@"longitude"];

// APINumber 3035812
//    [params setObject:@"35.426473" forKey:@"latitude"];
//    [params setObject:@"-118.995774" forKey:@"longitude"];

// APINumber 2963293
//    [params setObject:@"35.490424" forKey:@"latitude"];
//    [params setObject:@"-119.010642" forKey:@"longitude"];

    [params setObject:@"3" forKey:@"heading"];
    [params setObject:@"0" forKey:@"elevation"];
    [params setObject:@"0"forKey:@"altitude"];
    [params setObject:@"0" forKey:@"pitch"];

*/
    [params setObject:[CoreLocationController sharedInstance].latitude forKey:@"latitude"];
    [params setObject:[CoreLocationController sharedInstance].longitude forKey:@"longitude"];
    [params setObject:[NSString stringWithFormat:@"%f", [CoreLocationController sharedInstance].heading] forKey:@"heading"];
    [params setObject:[CoreLocationController sharedInstance].pitch forKey:@"elevation"];
    [params setObject:[CoreLocationController sharedInstance].altitude forKey:@"altitude"];
    [params setObject:[CoreLocationController sharedInstance].pitch forKey:@"pitch"];
    
    void(^_clearTabs)(void) = ^(void) {
        self.generalController = nil;
        self.mapController = nil;
        self.auditController = nil;
        self.searchController = nil;
        self.newWellController = nil;
    };
    
    void(^_notFound)(void) = ^(void) {
        self.wellData = nil;
        
        _clearTabs();
        [self clickTabButton:_tabMapButton];
        _tabGeneralButton.enabled = _tabAuditButton.enabled = NO;
        [self setTitleText];
    };
    
    [DSBezelActivityView newActivityViewForView:self.view];
    [[APIClient sharedInstance] callApi:kApiKeyWellDataByPoint httpMethod:kHTTPMethodGET parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        [DSBezelActivityView removeViewAnimated:NO];
        
        _debugLinkButton.hidden = YES;
        [_debugLinkButton setTitle:[operation.response.URL.absoluteString urldecode] forState:UIControlStateNormal];
        
        if ([[[responseObject objectForKey:@"result"] objectForKey:@"distance"] intValue] <= 30) {
            self.wellData = [responseObject objectForKey:@"result"];
            
            _tabGeneralButton.enabled = _tabAuditButton.enabled = YES;
            
            _clearTabs();
            [self clickTabButton:_tabGeneralButton];
        } else {
            _notFound();
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        [DSBezelActivityView removeViewAnimated:YES];
        
        _debugLinkButton.hidden = YES;
        [_debugLinkButton setTitle:[operation.response.URL.absoluteString urldecode] forState:UIControlStateNormal];
        
        _notFound();
    }];
}

- (IBAction)clickSubmit:(id)sender
{
    [_auditController saveAudit];
}

- (IBAction)clickDebugLink:(id)sender
{
    NSString *url = [_debugLinkButton titleForState:UIControlStateNormal];
    
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Do you want to open link in browser?" message:url delegate:self cancelButtonTitle:@"No" otherButtonTitles:@"Yes", nil];
    alert.tag = 1;
    [alert show];
    [alert release];
}


#pragma mark - UIAlertViewDelegate

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
    if (alertView.tag == 1 && buttonIndex == 1) {
        NSString *url = [_debugLinkButton titleForState:UIControlStateNormal];
        [[UIApplication sharedApplication] openURL:[NSURL URLWithString:url]];
    }
}


#pragma mark - TabViewControllerDelegate

- (void) tabController:(AbstractTabViewController *)controller performedAction:(NSDictionary *)dict
{
    NSString *action = [dict objectForKey:@"action"];
    if ([action isEqualToString:@"loadWellByAPI"]) {
        NSMutableDictionary *params = [NSMutableDictionary dictionary];
        [params setObject:[dict objectForKey:@"APINumber"] forKey:@"API"];
        
        [DSBezelActivityView newActivityViewForView:self.view];
        [[APIClient sharedInstance] callApi:kApiKeyWellDataByAPINumber httpMethod:kHTTPMethodGET parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
            [DSBezelActivityView removeViewAnimated:YES];
            
            _debugLinkButton.hidden = YES;
            [_debugLinkButton setTitle:[operation.response.URL.absoluteString urldecode] forState:UIControlStateNormal];
            
            self.wellData = [responseObject objectForKey:@"result"];
            
            _tabGeneralButton.enabled = _tabAuditButton.enabled = YES;
            
            void(^_clearTabs)(void) = ^(void) {
                self.generalController = nil;
                self.mapController = nil;
                self.auditController = nil;
                self.searchController = nil;
                self.newWellController = nil;
            };
            
            _clearTabs();
            [self clickTabButton:_tabGeneralButton];
        } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
            [DSBezelActivityView removeViewAnimated:YES];
            
            _debugLinkButton.hidden = YES;
            [_debugLinkButton setTitle:[operation.response.URL.absoluteString urldecode] forState:UIControlStateNormal];
            
            NSString *errorDesc = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
            
            if (error.code == 404) {
                errorDesc = [NSString stringWithFormat:@"We currently do not have any informations about API number: %@ in our database.", [dict objectForKey:@"APINumber"]];
            }
            
            UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorDesc message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
            [alert show];
            [alert release];
        }];
    } else if ([action isEqualToString:@"enableSaveButton"]) {
        _submitButton.enabled = [[dict objectForKey:@"enable"] boolValue];
    }
}

@end
