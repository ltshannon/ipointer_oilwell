//
//  SearchTabViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//




#import "AbstractTabViewController.h"




@interface SearchTabViewController : AbstractTabViewController
<UITextFieldDelegate>


@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIView *inputView;
@property (retain, nonatomic) IBOutlet UIImageView *searchTextFieldImage;
@property (retain, nonatomic) IBOutlet UITextField *searchTextField;
@property (retain, nonatomic) IBOutlet UIButton *buttonOk;



- (IBAction)actionButtonOkClicked:(id)sender;



@end
