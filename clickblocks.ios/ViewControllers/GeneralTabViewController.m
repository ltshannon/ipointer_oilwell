//
//  GeneralTabViewController.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "GeneralTabViewController.h"
#import "UIHelper.h"
#import <QuartzCore/QuartzCore.h>

@interface GeneralTabViewController ()

@property (retain, nonatomic) NSArray *keysForDisplaying;

@end

@implementation GeneralTabViewController

- (void)dealloc {
    [_scrollView release];
    [_keysForDisplaying release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setScrollView:nil];
    [super viewDidUnload];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view.
    
    CAGradientLayer *gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(_scrollView.left, _scrollView.top, _scrollView.width - 20, 10);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor whiteColor] CGColor], (id)[[UIColor colorWithWhite:1 alpha:0] CGColor], nil];
    [self.view.layer addSublayer:gradient];
    
    gradient = [CAGradientLayer layer];
    gradient.frame = CGRectMake(_scrollView.left, _scrollView.bottom - 10, _scrollView.width - 20, 10);
    gradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithWhite:1 alpha:0] CGColor], (id)[[UIColor whiteColor] CGColor], nil];
    [self.view.layer addSublayer:gradient];
    
    NSDictionary*(^_dict)(NSString*, NSString*) = ^(NSString *key, NSString *description) {
        return [NSDictionary dictionaryWithObjectsAndKeys:key, @"key", description, @"description", nil];
    };
    
    self.keysForDisplaying = [NSArray arrayWithObjects:
                              _dict(@"APINumber", @"API Number"),
                              _dict(@"WellNumber", @"Well Number"),
                              _dict(@"WellStatus", @"Well Status"),
                              _dict(@"LeaseName", @"Lease Name"),
                              _dict(@"Section", @"Section"),
                              _dict(@"Township", @"Township"),
                              _dict(@"Range", @"Range"),
                              _dict(@"LocationDe", @"Location Description"),
                              _dict(@"OperatorNa", @"Operator Name"),
                              _dict(@"FieldName", @"Oil Field Name"),
                              nil];
    
    [self fillScroll];
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void) fillScroll {
    NSMutableArray *array = [NSMutableArray array];
    
    [_keysForDisplaying enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *key = [obj objectForKey:@"key"];
        NSString *desc = [obj objectForKey:@"description"];
        NSString *value = [[self.wellData objectForKey:@"well"] objectForKey:key];
        if ([value isKindOfClass:[NSNull class]]) value = @"";
        
        [array addObject:[NSDictionary dictionaryWithObjectsAndKeys:desc, @"key", [value description], @"value", nil]];
    }];
    
    for (UIView *v in _scrollView.subviews) {
        if ([v isKindOfClass:[UILabel class]]) {
            [v removeFromSuperview];
        }
    }
    
    NSInteger row = -1;
    NSInteger column = 0;
    CGFloat firstRowWidth = 350;
    CGFloat secondRowWidth = 400;
    for (int i = 0; i < [array count]; i++) {
        NSDictionary *dict = [array objectAtIndex:i];
        
        if (i%2 == 0) row++;
        column = (int)(i%2 != 0);
        
        CGFloat width = (column ? secondRowWidth : firstRowWidth);
        CGFloat x = 30 + column * width;
        CGFloat y = 30 + row * 60;
        
        UILabel *label = [[UILabel alloc] initWithFrame:CGRectMake(x, y, width, 21)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:kGillSansStdBoldCondensed size:17];
        label.textColor = [UIColor blackColor];
        label.text = [dict objectForKey:@"key"];
        [_scrollView addSubview:label];
        [label release];
        
        label = [[UILabel alloc] initWithFrame:CGRectMake(x, y + 26, width, 21)];
        label.backgroundColor = [UIColor clearColor];
        label.font = [UIFont fontWithName:kGillSansStdCondensed size:17];
        label.textColor = [UIColor blackColor];
        label.text = [dict objectForKey:@"value"];
        [_scrollView addSubview:label];
        [label release];
    }
    
    _scrollView.contentSize = CGSizeMake(_scrollView.width, 30 + ceilf([array count]/2.0) * 60 + 20);
}

@end
