//
//  LoginViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UIView *loginView;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UITextField *userTextField;
@property (retain, nonatomic) IBOutlet UITextField *passwordTextField;
@property (retain, nonatomic) IBOutlet UIImageView *userTextFieldImage;
@property (retain, nonatomic) IBOutlet UIImageView *passwordTextFieldImage;
@property (retain, nonatomic) IBOutlet UIButton *rememberButton;
@property (retain, nonatomic) IBOutlet UIButton *loginButton;
@property (retain, nonatomic) IBOutletCollection(UILabel) NSArray *labels;

- (IBAction)clickRemember:(id)sender;
- (IBAction)clickLogin:(id)sender;

@end
