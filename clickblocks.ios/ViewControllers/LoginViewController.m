//
//  LoginViewController.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "LoginViewController.h"
#import "UIHelper.h"
#import "APIClient.h"
#import "Common.h"
#import "DataManager.h"
#import "Constants.h"
#import "DSActivityView.h"


@implementation LoginViewController




#pragma mark - Lifecycle


- (void)dealloc {
    [_userTextField release];
    [_passwordTextField release];
    [_rememberButton release];
    [_loginButton release];
    [_loginView release];
    [_labels release];
    [_userTextFieldImage release];
    [_passwordTextFieldImage release];
    [_contentView release];
    [super dealloc];
}

- (void)viewDidUnload {
    [self setUserTextField:nil];
    [self setPasswordTextField:nil];
    [self setRememberButton:nil];
    [self setLoginButton:nil];
    [self setLoginView:nil];
    [self setLabels:nil];
    [self setUserTextFieldImage:nil];
    [self setPasswordTextFieldImage:nil];
    [self setContentView:nil];
    [super viewDidUnload];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.userTextField.font = [UIFont fontWithName:@"GillSans-CondensedBold" size:25];
    self.passwordTextField.font = [UIFont fontWithName:@"GillSans-CondensedBold" size:25];
    self.userTextFieldImage.image = [UIHelper loginTextFieldImage];
    self.passwordTextFieldImage.image = [UIHelper loginTextFieldImage];
    
    self.loginButton.titleLabel.font = [UIFont fontWithName:@"GillSans-CondensedBold" size:25];
    [self.loginButton setBackgroundImage:[UIHelper bigYellowButtonImage] forState:UIControlStateNormal];
    
    _rememberButton.selected = [[[NSUserDefaults standardUserDefaults] objectForKey:kRememberedRememberFlagDataKey] boolValue];
    if (_rememberButton.selected) {
        _userTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kRememberedEmailDataKey];
        _passwordTextField.text = [[NSUserDefaults standardUserDefaults] objectForKey:kRememberedPasswordDataKey];
    }
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [super viewDidDisappear:animated];
}


- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}




#pragma mark - Actions

- (IBAction)clickRemember:(id)sender
{
    _rememberButton.selected = !_rememberButton.selected;
}




- (IBAction)clickLogin:(id)sender
{
    BOOL remember = self.rememberButton.selected;

    NSString *email = [_userTextField.text trim];
    NSString *password = [_passwordTextField.text trim];
    
    if (!email.length || !password.length) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Please enter email and password" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    } else {
        [DSBezelActivityView newActivityViewForView:self.view];
        [[APIClient sharedInstance] loginWithEmail:email password:password completion:^(NSError *error)
         {
            [DSBezelActivityView removeViewAnimated:NO];
            if (!error) {
                if (remember) {
                    [[NSUserDefaults standardUserDefaults] setObject:email forKey:kRememberedEmailDataKey];
                    [[NSUserDefaults standardUserDefaults] setObject:password forKey:kRememberedPasswordDataKey];
                } else {
                    [[DataManager sharedInstance] clearRememberedCredentials];
                }
                [[NSUserDefaults standardUserDefaults] setObject:[NSNumber numberWithBool:remember] forKey:kRememberedRememberFlagDataKey];
                [[NSUserDefaults standardUserDefaults] synchronize];
                
                [self dismissViewControllerAnimated:YES completion:nil];
            } else {
                NSString *errorDesc = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorDesc delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                [alert show];
                [alert release];
            }
        }];
    }
}




#pragma mark - UITextFieldDelegate


- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if ( textField == _userTextField )
    {
        [_passwordTextField becomeFirstResponder];
    }
    else if ( textField == _passwordTextField )
    {
        [_passwordTextField resignFirstResponder];
        [self clickLogin:nil];
    }
    
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if ( textField == _userTextField )
    {
        self.userTextFieldImage.image = [UIHelper loginTextFieldSelectedImage];
    }
    else if ( textField == _passwordTextField )
    {
        self.passwordTextFieldImage.image = [UIHelper loginTextFieldSelectedImage];
    }
}


- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if ( textField == _userTextField )
    {
        self.userTextFieldImage.image = [UIHelper loginTextFieldImage];
    }
    else if ( textField == _passwordTextField )
    {
        self.passwordTextFieldImage.image = [UIHelper loginTextFieldImage];
    }
}




#pragma mark - KeyboardDetect


- (void)keyboardWillHide: (NSNotification *)notif
{
    CGFloat duration = [[notif.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:duration animations:^{
        _contentView.top = 0;
    }];
}


- (void)keyboardWillShow: (NSNotification *)notif
{
    CGFloat duration = [[notif.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];

    [UIView animateWithDuration:duration animations:^{
        _contentView.top = -150;
    }];
}




@end
