//
//  AuditTabViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "AbstractTabViewController.h"
#import "ChemicalsPopoverContentViewController.h"

@interface AuditTabViewController : AbstractTabViewController <UITableViewDataSource, UITableViewDelegate, UITextViewDelegate, UIPopoverControllerDelegate, ChemicalsPopoverContentViewControllerDelegate>

@property (retain, nonatomic) IBOutlet UIButton *chemicalButton;
@property (retain, nonatomic) IBOutlet UITableView *tableView;
@property (retain, nonatomic) IBOutlet UITextView *textView;
@property (retain, nonatomic) IBOutlet UILabel *placeholderLabel;
@property (retain, nonatomic) NSMutableArray *properties;
@property (retain, nonatomic) NSArray *chemicals;
@property (assign, nonatomic) NSInteger selectedChemicalIndex;
@property (retain, nonatomic) UIPopoverController *chemicalPopoverController;
@property (retain, nonatomic) IBOutlet UILabel *dateLabel;
@property (retain, nonatomic) IBOutlet UILabel *nameLabel;

- (IBAction)clickChemical:(id)sender;
- (void) loadAudit;
- (void) saveAudit;

@end
