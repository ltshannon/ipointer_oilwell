//
//  ViewController.h
//  clickblocks.ios
//
//  Created by Vitaly Dubov on 9/27/12.
//  Copyright (c) 2012 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController <UITextFieldDelegate>

@property (retain, nonatomic) IBOutlet UITextField *apiTextField;
@property (retain, nonatomic) IBOutlet UIButton *apiButton;
@property (retain, nonatomic) IBOutlet UIButton *pointButton;
@property (retain, nonatomic) IBOutlet UIView *bottomView;
@property (retain, nonatomic) IBOutlet UIView *loadingView;
@property (retain, nonatomic) IBOutlet UIView *loadingFinishedView;

- (IBAction)clickApi:(id)sender;
- (IBAction)clickPoint:(id)sender;

@end
