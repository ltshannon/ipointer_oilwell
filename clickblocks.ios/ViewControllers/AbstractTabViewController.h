//
//  AbstractTabViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class AbstractTabViewController;
@protocol TabViewControllerDelegate <NSObject>
@optional
- (void) tabController: (AbstractTabViewController *) controller performedAction: (NSDictionary *) dict;
@end

@interface AbstractTabViewController : UIViewController

@property (retain, nonatomic) NSDictionary                          *wellData;
@property (assign, nonatomic) NSObject<TabViewControllerDelegate>   *delegate;

@end
