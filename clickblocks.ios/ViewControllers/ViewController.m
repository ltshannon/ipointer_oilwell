//
//  ViewController.m
//  clickblocks.ios
//
//  Created by Vitaly Dubov on 9/27/12.
//  Copyright (c) 2012 Saritasa. All rights reserved.
//

#import "ViewController.h"
#import "Constants.h"
#import "APIClient.h"
#import "InformationViewController.h"
#import "DSActivityView.h"
#import "coreLocationController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)dealloc {
    [[NSNotificationCenter defaultCenter] removeObserver:self];
    
    [_apiTextField release];
    [_apiButton release];
    [_pointButton release];
    [_bottomView release];
    [_loadingView release];
    [_loadingFinishedView release];
    [super dealloc];
}

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationUpdated:) name:NOTIF_LocationOK object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(locationUpdated:) name:NOTIF_LocationNotOK object:nil];
    
    DLog(@"startUpdatingLocation");
    [[CoreLocationController sharedInstance] createLocation];
    [[CoreLocationController sharedInstance] startUpdatingLocation];
}

- (void)viewDidUnload
{
    [self setApiTextField:nil];
    [self setApiButton:nil];
    [self setPointButton:nil];
    [self setBottomView:nil];
    [self setLoadingView:nil];
    [self setLoadingFinishedView:nil];
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return UIInterfaceOrientationIsLandscape(interfaceOrientation);
}

- (void) locationUpdated: (NSNotification *) notification {
    _loadingView.hidden = YES;
    _loadingFinishedView.hidden = NO;
    _bottomView.hidden = NO;
    
    if ([notification.name isEqualToString:NOTIF_LocationNotOK]) {
        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Low location accuracy" message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
        [alert show];
        [alert release];
    }
}

- (IBAction)clickApi:(id)sender {
    [self showInformation:nil];
}

- (IBAction)clickPoint:(id)sender
{

    NSMutableDictionary *params = [NSMutableDictionary dictionary];

    [params setObject:[CoreLocationController sharedInstance].latitude forKey:@"latitude"];
    [params setObject:[CoreLocationController sharedInstance].longitude forKey:@"longitude"];
    [params setObject:[NSString stringWithFormat:@"%f", [CoreLocationController sharedInstance].heading] forKey:@"heading"];
    [params setObject:[CoreLocationController sharedInstance].pitch forKey:@"elevation"];
    [params setObject:[CoreLocationController sharedInstance].altitude forKey:@"altitude"];
    [params setObject:[CoreLocationController sharedInstance].pitch forKey:@"pitch"];

// LTS 9/22/2013 Hardcode a data point.
/*
    // APINumber 2976966
    [params setObject:@"35.313269" forKey:@"latitude"];
    [params setObject:@"-119.064432" forKey:@"longitude"];
    
    // APINumber 2900346
    //    [params setObject:@"35.341976" forKey:@"latitude"];
    //    [params setObject:@"-118.908783" forKey:@"longitude"];
    
    // APINumber 3035812
    //    [params setObject:@"35.426473" forKey:@"latitude"];
    //    [params setObject:@"-118.995774" forKey:@"longitude"];
    
    // APINumber 2963293
    //    [params setObject:@"35.490424" forKey:@"latitude"];
    //    [params setObject:@"-119.010642" forKey:@"longitude"];
    
    [params setObject:@"3" forKey:@"heading"];
    [params setObject:@"0" forKey:@"elevation"];
    [params setObject:@"0"forKey:@"altitude"];
    [params setObject:@"0" forKey:@"pitch"];
*/
    [DSBezelActivityView newActivityViewForView:self.view];
    [[APIClient sharedInstance] callApi:kApiKeyWellDataByPoint httpMethod:kHTTPMethodGET parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject)
    {
        [DSBezelActivityView removeViewAnimated:YES];
        
        if ([[[responseObject objectForKey:@"result"] objectForKey:@"distance"] intValue] <= 30)
        {
            [self showInformation:[responseObject objectForKey:@"result"]];
        }
        else
        {
            [self showInformation:nil];
        }
    } failure:^(AFHTTPRequestOperation *operation, NSError *error)
    {
        [DSBezelActivityView removeViewAnimated:YES];
        
        [self showInformation:nil];
    }];
}

- (void) showInformation: (id) data
{
    InformationViewController *controller;
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        controller = [[InformationViewController alloc] initWithNibName:@"InformationViewController" bundle:nil];
    }
    else
    {
        controller = [[InformationViewController alloc] initWithNibName:@"InformationViewController_iPhone" bundle:nil];
    }
    controller.wellData = data;
    controller.modalPresentationStyle = UIModalPresentationFullScreen;
    [self presentViewController:controller animated:YES completion:nil];
    [controller release];
}


#pragma mark - UITextFieldDelegate

- (BOOL) textFieldShouldReturn:(UITextField *)textField {
    [_apiTextField resignFirstResponder];
    [self clickApi:nil];
    
    return YES;
}

@end
