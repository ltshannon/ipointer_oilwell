//
//  NewWellViewController.m
//  OilWell
//
//  Created by LAWRENCE SHANNON on 9/28/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "NewWellViewController.h"
#import "AppDelegate.h"

@interface NewWellViewController ()

@end

@implementation NewWellViewController

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

}

- (void) viewWillAppear:(BOOL)animated
{
    
    AppDelegate *appD = (id) [[UIApplication sharedApplication] delegate];
    
    NSMutableString *myString = [[NSMutableString alloc] init];
    for (NSMutableArray *str in appD.timeStrings)
    {
        [myString appendString:(NSString *)str];
        [myString appendString:@"\n"];
    }
    
    NSLog(@"String: %@", myString);
    self.myTextView.text = myString;
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_myScrollView release];
    [_myTextView release];
    [super dealloc];
}
- (void)viewDidUnload {
    [self setMyScrollView:nil];
    [self setMyTextView:nil];
    [super viewDidUnload];
}
@end
