//
//  SearchTabViewController.m
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//




#import "SearchTabViewController.h"
#import "UIHelper.h"
#import "Common.h"



@implementation SearchTabViewController



#pragma mark - Lifecycle


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}


- (void)viewDidLoad
{
    [super viewDidLoad];
    
    self.inputView.center = self.contentView.center;
	
    self.searchTextField.font = [UIFont fontWithName:@"GillSans-CondensedBold" size:25];
    self.searchTextFieldImage.image = [UIHelper loginTextFieldImage];
    self.buttonOk.titleLabel.font = [UIFont fontWithName:@"GillSans-CondensedBold" size:25];
    [self.buttonOk setBackgroundImage:[UIHelper bigYellowButtonImage] forState:UIControlStateNormal];
}


- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)dealloc {
    [_searchTextFieldImage release];
    [_searchTextField release];
    [_buttonOk release];
    [_inputView release];
    [_contentView release];
    [super dealloc];
}


- (void)viewDidUnload {
    [self setSearchTextFieldImage:nil];
    [self setSearchTextField:nil];
    [self setButtonOk:nil];
    [self setInputView:nil];
    [self setContentView:nil];
    [super viewDidUnload];
}


- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}


- (void) viewDidDisappear:(BOOL)animated
{
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillShowNotification
                                                  object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self
                                                    name:UIKeyboardWillHideNotification
                                                  object:nil];
    
    [super viewDidDisappear:animated];
}




#pragma mark - Actions


- (IBAction)actionButtonOkClicked:(id)sender
{
    [self.searchTextField resignFirstResponder];
    [self search:self.searchTextField.text];
}




#pragma mark - Logic


- (void) search:(NSString *)text
{
    NSString *_text = [text trim];
    if (![_text length]) {
        
    } else {
        if (self.delegate && [self.delegate respondsToSelector:@selector(tabController:performedAction:)]) {
            NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:@"loadWellByAPI", @"action", _text, @"APINumber", nil];
            [self.delegate tabController:self performedAction:dict];
        }
    }
}




#pragma mark - UITextFieldDelegate


- (BOOL) textFieldShouldReturn:(UITextField *)textField
{
    if ( textField == self.searchTextField )
    {
        [self.searchTextField resignFirstResponder];
        [self search:self.searchTextField.text];
    }
    
    
    return YES;
}


- (void) textFieldDidBeginEditing:(UITextField *)textField
{
    if ( textField == self.searchTextField )
    {
        self.searchTextFieldImage.image = [UIHelper loginTextFieldSelectedImage];
    }
}


- (void) textFieldDidEndEditing:(UITextField *)textField
{
    if ( textField == self.searchTextField )
    {
        self.searchTextFieldImage.image = [UIHelper loginTextFieldImage];
    }
}




#pragma mark - KeyboardDetect


- (void)keyboardWillHide: (NSNotification *)notif
{
    CGFloat duration = [[notif.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    [UIView animateWithDuration:duration animations:^{
        CGPoint newCenter = self.contentView.center;
        self.inputView.center = newCenter;
    }];
}


- (void)keyboardWillShow: (NSNotification *)notif
{
    CGFloat duration = [[notif.userInfo objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [UIView animateWithDuration:duration animations:^{
        CGPoint newCenter = self.contentView.center;
        newCenter.y -= 100;
        self.inputView.center = newCenter;
    }];
}




@end
