//
//  GeneralTabViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import "AbstractTabViewController.h"

@interface GeneralTabViewController : AbstractTabViewController <UIScrollViewDelegate>

@property (retain, nonatomic) IBOutlet UIScrollView *scrollView;

- (void) fillScroll;

@end
