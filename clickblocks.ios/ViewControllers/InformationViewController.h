//
//  InformationViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/6/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AbstractTabViewController.h"

@class GeneralTabViewController;
@class MapTabViewController;
@class AuditTabViewController;
@class SearchTabViewController;
@class NewWellViewController;
@interface InformationViewController : UIViewController <TabViewControllerDelegate, UIAlertViewDelegate>

@property (retain, nonatomic) IBOutlet UIButton *tabGeneralButton;
@property (retain, nonatomic) IBOutlet UIButton *tabMapButton;
@property (retain, nonatomic) IBOutlet UIButton *tabAuditButton;
@property (retain, nonatomic) IBOutlet UIButton *tabSearchButton;
@property (retain, nonatomic) IBOutlet UIButton *tabNewWellButton;
@property (retain, nonatomic) IBOutlet UIButton *pointButton;
@property (retain, nonatomic) IBOutlet UIView *contentView;
@property (retain, nonatomic) IBOutlet UIImageView *cornerFixView;
@property (retain, nonatomic) IBOutlet UIButton *submitButton;
@property (retain, nonatomic) IBOutlet UILabel *titleLabel;
@property (retain, nonatomic) IBOutlet UILabel *distanceLabel;
@property (retain, nonatomic) IBOutlet UIButton *debugLinkButton;
@property (retain, nonatomic) GeneralTabViewController *generalController;
@property (retain, nonatomic) MapTabViewController *mapController;
@property (retain, nonatomic) AuditTabViewController *auditController;
@property (retain, nonatomic) SearchTabViewController *searchController;
@property (retain, nonatomic) NewWellViewController *newWellController;
@property (retain, nonatomic) AbstractTabViewController *currentController;
@property (retain, nonatomic) NSDictionary *wellData;

- (IBAction)clickTabButton:(UIButton *)sender;
- (IBAction)clickPoint:(id)sender;
- (IBAction)clickSubmit:(id)sender;
- (IBAction)clickDebugLink:(id)sender;
- (void) setTitleText;

@end
