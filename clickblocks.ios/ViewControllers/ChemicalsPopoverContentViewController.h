//
//  ChemicalsPopoverContentViewController.h
//  OilWell
//
//  Created by Vitaly Dubov on 2/14/13.
//  Copyright (c) 2013 Saritasa. All rights reserved.
//

#import <UIKit/UIKit.h>

@class ChemicalsPopoverContentViewController;
@protocol ChemicalsPopoverContentViewControllerDelegate <NSObject>
@optional
- (void) chemicalsPopoverContentViewController: (ChemicalsPopoverContentViewController *) controller didSelectIndex: (NSInteger) index;
@end


@interface ChemicalsPopoverContentViewController : UITableViewController

@property (retain, nonatomic) NSArray *chemicals;
@property (assign, nonatomic) NSInteger selectedChemicalIndex;
@property (assign, nonatomic) NSObject<ChemicalsPopoverContentViewControllerDelegate> *delegate;
@property (assign, nonatomic) UIPopoverController *popover;

@end
