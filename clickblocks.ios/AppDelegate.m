//
//  AppDelegate.m
//  clickblocks.ios
//
//  Created by Vitaly Dubov on 9/27/12.
//  Copyright (c) 2012 Saritasa. All rights reserved.
//

#import "AppDelegate.h"
#import "ViewController.h"
#import "Common.h"
#import "APIClient.h"
#import "DSActivityView.h"

@implementation AppDelegate

- (void)dealloc
{
    [_window release];
    [_viewController release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    DLog(@"documents path = %@", [Common documentsDir]);
    
    [Common importFonts];
    [Common printAvailableFonts];
//    [self loadDataBase];
    
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    // Override point for customization after application launch.
    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController" bundle:nil];
    }
    else
    {
        self.viewController = [[ViewController alloc] initWithNibName:@"ViewController_iPhone" bundle:nil];
    }
    
    self.window.rootViewController = self.viewController;

    if ([UIDevice currentDevice].userInterfaceIdiom == UIUserInterfaceIdiomPad)
    {
        self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController" bundle:nil];
    }
    else
    {
        self.loginViewController = [[LoginViewController alloc] initWithNibName:@"LoginViewController_iPhone" bundle:nil];
    }
    
    [self.window makeKeyAndVisible];

    self.loginViewController.modalPresentationStyle = UIModalPresentationFullScreen;

    [self.viewController presentViewController:self.loginViewController animated:NO completion:nil];

    self.timeStrings = [[NSMutableArray alloc] init];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later. 
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
}

- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

- (void) loadDataBase
{
    
    oilWellDB = [[OilWellDB alloc] initialise];
    if (![[NSUserDefaults standardUserDefaults] boolForKey: @"OilWellDB"])
    {
        [self initLookupChemicalPropertiesDB];
        [self initWellAudit];
        
        
        [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"OilWellDB"];
        [[NSUserDefaults standardUserDefaults] synchronize];
        [[APIClient sharedInstance] loginWithEmail:@"user1@test.com" password:@"12345" completion:^(NSError *error)
         {
             if (!error)
             {
                 NSString *path = [[NSBundle mainBundle] pathForResource:@"APINumbers" ofType:@"txt"];
                 NSString *fileContents = [NSString stringWithContentsOfFile:path encoding:NSUTF8StringEncoding error:nil];
                 self.allLinedStrings = [fileContents componentsSeparatedByCharactersInSet:[NSCharacterSet newlineCharacterSet]];
                 self.apiCount = 0;
                 [DSBezelActivityView newActivityViewForView:self.self.loginViewController.view];
                 [self initWells];
             } else
             {
                 NSString *errorDesc = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
                 UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error" message:errorDesc delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
                 [alert show];
                 [alert release];
             }
             
         }];
        
    }
    
}

- (void) initWells
{
    
    if (self.apiCount < self.allLinedStrings.count)
    {
        NSString* apiNumber = [self.allLinedStrings objectAtIndex:self.apiCount];
        NSDictionary *dict = [NSDictionary dictionaryWithObjectsAndKeys:apiNumber, @"API", nil];
        
        [self loadWellDB:dict];
        self.apiCount++;
    }
    else
    {
        [DSBezelActivityView removeViewAnimated:NO];
    }
}

- (void) loadWellDB: (NSDictionary *) dict
{
    
    [[APIClient sharedInstance] callApi:kApiKeyWellDataByAPINumber
                             httpMethod:kHTTPMethodGET
                             parameters:dict
                                success:^(AFHTTPRequestOperation *operation, id responseObject)
     {
         NSDictionary *well = [responseObject objectForKey:@"result"];
         NSDictionary *wellData = [well objectForKey:@"well"];
         [oilWellDB insertWells:@""
                     DistrictNu: [[wellData objectForKey:@"DistrictNu"] description]
                      APINumber: [[wellData objectForKey:@"APINumber"] description]
                        BLMWell: [[wellData objectForKey:@"BLMWell"] description]
                     RedrillCan: [[wellData objectForKey:@"RedrillCan:"] description]
                        DryHole: [[wellData objectForKey:@"DryHole"] description]
                     WellStatus: [[wellData objectForKey:@"WellStatus"] description]
                     OperatorNa: [[wellData objectForKey:@"OperatorNa"] description]
                     CountyName: [[wellData objectForKey:@"CountyName"] description]
                      FieldName: [[wellData objectForKey:@"FieldName"] description]
                       AreaName: [[wellData objectForKey:@"AreaName"] description]
                        section: [[[wellData objectForKey:@"section"] description] doubleValue]
                       Township: [[wellData objectForKey:@"Township"] description]
                          Range: [[wellData objectForKey:@"Range"] description]
                     BaseMeridi: [[wellData objectForKey:@"BaseMeridi"] description]
                      Elevation: [[wellData objectForKey:@"Elevation"] description]
                     LocationDe: [[[wellData objectForKey:@"LocationDe"] description] doubleValue]
                       Latitude: [[[wellData objectForKey:@"Latitude"] description] doubleValue]
                      Longitude: [[[wellData objectForKey:@"Longitude"] description] doubleValue]
                     GISSourceC: [[wellData objectForKey:@"GISSourceC"] description]
                      LeaseName: [[wellData objectForKey:@"LeaseName"] description]
                     WellNumber: [[wellData objectForKey:@"WellNumber"] description]
                        EPAWell: [[wellData objectForKey:@"EPAWell"] description]
                     Hydraulica: [[wellData objectForKey:@"Hydraulica"] description]
                     Confidenti: [[wellData objectForKey:@"Confidenti"] description]
                       SPUDDate: [[wellData objectForKey:@"SPUDDate"] description]
                     WellDepthA: [[wellData objectForKey:@"WellDepthA"] description]
                     RedrillFoo: [[wellData objectForKey:@"RedrillFoo"] description]
                     AbandonedD: [[wellData objectForKey:@"AbandonedD"] description]
                     Completion: [[wellData objectForKey:@"Completion"] description]
                      GISSymbol: [[wellData objectForKey:@"GISSymbol"] description]
                         API_10: [[wellData objectForKey:@"API_10"] description]
                        OS_AREA: [[wellData objectForKey:@"OS_AREA"] description]
                            GPS: [[[wellData objectForKey:@"GPS"] description] doubleValue]
                           WELL: [[wellData objectForKey:@"WELL"] description]
                    WELL_STATUS: [[wellData objectForKey:@"WELL_STATUS"] description]
                       CHEMICAL: [[wellData objectForKey:@"CHEMICAL"] description]
                SETUP_CONDITION: [[wellData objectForKey:@"SETUP_CONDITION"] description]
                         LABELS: [[wellData objectForKey:@"LABELS"] description]
                  VISABLE_LEAKS: [[wellData objectForKey:@"VISABLE_LEAKS"] description]
             CONTAIN_MENT_CLEAN: [[wellData objectForKey:@"CONTAIN_MENT_CLEAN"] description]
                  ELECTRIC_SAFE: [[wellData objectForKey:@"ELECTRIC_SAFE"] description]
             NO_TRIPPING_HAZARD: [[wellData objectForKey:@"NO_TRIPPING_HAZARD"] description]
                           DATE: [[wellData objectForKey:@"DATE"] description]
                       COMMENTS: [[wellData objectForKey:@"COMMENTS"] description]
                      AuditUser: [[wellData objectForKey:@"AuditUser"] description]];
         
         [self performSelector:@selector(initWells) withObject:nil];
         
     }
                                failure:^(AFHTTPRequestOperation *operation, NSError *error)
     {
         [DSBezelActivityView removeViewAnimated:NO];
         NSString *errorDesc = [error.userInfo objectForKey:NSLocalizedDescriptionKey];
         
         if (error.code == 404) {
             errorDesc = [NSString stringWithFormat:@"We currently do not have any informations about API number: %@ in our database.", [dict objectForKey:@"APINumber"]];
         }
         
         UIAlertView *alert = [[UIAlertView alloc] initWithTitle:errorDesc message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
         [alert show];
         [alert release];
     }];
    //             }
}

- (void) initWellAuditProperties
{
    
    /*
     [oilWellDB insertDataWellAuditProperties:1 propertyID:2 success:1 comment:@"THis is David\'s comment on WellAuditProperties"];
     nsertDataWellAuditProperties: (int64_t) auditID
     propertyID:
     success:
     comment:
     
     [oilWellDB
     insertDataWellAuditProperties:8 propertyID:1,1,NULL),
     [oilWellDB
     insertDataWellAuditProperties:8 propertyID:2,1,NULL),
     [oilWellDB
     insertDataWellAuditProperties:8 propertyID:3,1,NULL),
     [oilWellDB
     insertDataWellAuditProperties:8 propertyID:4,1,NULL),
     [oilWellDB
     insertDataWellAuditProperties:8 propertyID:5,0,'This is a test.\n\n\nThis is another line'),
     [oilWellDB
     insertDataWellAuditProperties:8 propertyID:6,1,NULL),
     
     */
}

- (void) initWellAudit
{
    
    [oilWellDB insertDataWellAudit:1 wellAPI:@"32393736393636" createdBy:1 created:@"2013-09-27 16:56:40" comments:@"54686973206973206461766964277320636F6D6D656E742E" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:6 wellAPI:@"32393736393637" createdBy:1 created:@"2013-09-29 17:18:53" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:7 wellAPI:@"32393736393637" createdBy:1 created:@"2013-09-29 17:19:09" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:8 wellAPI:@"32393736393637" createdBy:1 created:@"2013-09-29 17:19:51" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:9 wellAPI:@"32393736393637" createdBy:1 created:@"2013-09-29 17:20:36" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:10 wellAPI:@"32393736393636" createdBy:1 created:@"2013-09-29 17:20:49" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:11 wellAPI:@"32393736393636" createdBy:3 created:@"2013-09-29 17:24:21" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:12 wellAPI:@"32393736393636" createdBy:3 created:@"2013-09-29 17:24:58" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:13 wellAPI:@"32393736393636" createdBy:3 created:@"2013-09-29 17:26:25" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:14 wellAPI:@"32393736393636" createdBy:3 created:@"2013-09-29 17:27:17" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:15 wellAPI:@"32393736393637" createdBy:1 created:@"2013-09-29 17:39:32" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:16 wellAPI:@"32393736393636" createdBy:1 created:@"2013-09-29 17:41:38" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:17 wellAPI:@"32393736393637" createdBy:1 created:@"2013-09-29 17:41:58" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:18 wellAPI:@"32393736393636" createdBy:1 created:@"2013-09-29 18:27:14" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:19 wellAPI:@"32393736393636" createdBy:1 created:@"2013-09-29 18:35:53" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:20 wellAPI:@"32393736393636" createdBy:1 created:@"2013-09-29 18:36:03" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:21 wellAPI:@"32393736393636" createdBy:1 created:@"2013-09-29 18:43:45" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:22 wellAPI:@"32393538333431" createdBy:3 created:@"2013-09-30 11:19:19" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:23 wellAPI:@"32393538333431" createdBy:3 created:@"2013-09-30 11:20:29" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:24 wellAPI:@"32393234383736" createdBy:3 created:@"2013-09-30 19:59:27" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:25 wellAPI:@"33303337343834" createdBy:3 created:@"2013-10-02 12:31:44" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:26 wellAPI:@"32393130363537" createdBy:3 created:@"2013-10-03 14:22:58" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:27 wellAPI:@"32393434303532" createdBy:3 created:@"2013-10-03 14:43:29" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:28 wellAPI:@"32393430323438" createdBy:3 created:@"2013-10-03 14:44:35" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:29 wellAPI:@"32393734333036" createdBy:3 created:@"2013-10-03 14:46:46" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:30 wellAPI:@"32393130313531" createdBy:3 created:@"2013-10-03 14:49:15" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:31 wellAPI:@"32393437393230" createdBy:3 created:@"2013-10-04 13:53:45" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:32 wellAPI:@"32393030313034" createdBy:3 created:@"2013-10-04 13:55:53" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:33 wellAPI:@"32393434343933" createdBy:3 created:@"2013-10-04 14:04:08" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:34 wellAPI:@"32393630373833" createdBy:3 created:@"2013-10-04 14:09:22" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:35 wellAPI:@"32393537373534" createdBy:3 created:@"2013-10-04 14:13:37" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:36 wellAPI:@"32393437393230" createdBy:3 created:@"2013-10-10 15:56:33" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:37 wellAPI:@"33303130323738" createdBy:3 created:@"2013-10-10 16:11:56" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:38 wellAPI:@"33303233383037" createdBy:3 created:@"2013-10-10 16:20:04" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:39 wellAPI:@"33303239373936" createdBy:3 created:@"2013-10-10 16:28:30" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:40 wellAPI:@"33303130373934" createdBy:3 created:@"2013-10-10 16:39:29" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:41 wellAPI:@"33303130373934" createdBy:3 created:@"2013-10-10 16:55:05" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
    [oilWellDB insertDataWellAudit:42 wellAPI:@"32393736393636" createdBy:1 created:@"2013-10-11 07:45:14" comments:@"" auditID:0 APINumber:@"" chemicalType:@"" property:@"" userEmail:@"" datetime:@"0000-00-00 00:00:00"];
}

- (void) initLookupChemicalPropertiesDB
{
    
    [oilWellDB insertDataLookupChemicalProperties:1 name:@"Set Up Conditions" chemicalType:@"PAW4HF"];
    [oilWellDB insertDataLookupChemicalProperties:2 name:@"Labels" chemicalType:@"PAW4HF"];
	[oilWellDB insertDataLookupChemicalProperties:3 name:@"Visible Leaks" chemicalType:@"PAW4HF"];
	[oilWellDB insertDataLookupChemicalProperties:4 name:@"Containment Clean" chemicalType:@"PAW4HF"];
	[oilWellDB insertDataLookupChemicalProperties:5 name:@"Electric Safe" chemicalType:@"PAW4HF"];
	[oilWellDB insertDataLookupChemicalProperties:6 name:@"Tripping Hazard" chemicalType:@"PAW4HF"];
	[oilWellDB insertDataLookupChemicalProperties:7 name:@"Set Up Conditions" chemicalType:@"WCW4527"];
	[oilWellDB insertDataLookupChemicalProperties:8 name:@"Labels" chemicalType:@"WCW4527"];
	[oilWellDB insertDataLookupChemicalProperties:9 name:@"Visible Leaks" chemicalType:@"WCW4527"];
	[oilWellDB insertDataLookupChemicalProperties:10 name:@"Containment Clean" chemicalType:@"WCW4527"];
	[oilWellDB insertDataLookupChemicalProperties:11 name:@"Electric Save" chemicalType:@"WCW4527"];
	[oilWellDB insertDataLookupChemicalProperties:12 name:@"Tripping Hazard" chemicalType:@"WCW4527"];
	[oilWellDB insertDataLookupChemicalProperties:13 name:@"Set Up Conditions" chemicalType:@"CRW9257"];
	[oilWellDB insertDataLookupChemicalProperties:14 name:@"Labels" chemicalType:@"CRW9257"];
	[oilWellDB insertDataLookupChemicalProperties:15 name:@"Visible Leaks" chemicalType:@"CRW9257"];
	[oilWellDB insertDataLookupChemicalProperties:16 name:@"Containment Clean" chemicalType:@"CRW9257"];
	[oilWellDB insertDataLookupChemicalProperties:17 name:@"Electric Save" chemicalType:@"CRW9257"];
	[oilWellDB insertDataLookupChemicalProperties:18 name:@"Tripping Hazard" chemicalType:@"CRW9257"];
	[oilWellDB insertDataLookupChemicalProperties:19 name:@"Signage: Location clearly numbered and posted?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:21 name:@"USL well signs have 1/4 section, township, range and lease#?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:22 name:@"Automatic start sign affixed and legible?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:23 name:@"Caution HOT sign installed?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:24 name:@"All electrical boxes and switch boxes have proper covers?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:25 name:@"All electrical equipment has proper identification voltage and warning labels?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:26 name:@"Ladders>20 ft must be caged or have a rest platform." chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:27 name:@"Ladder platforms must have drop bars or safety chains." chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:28 name:@"Ladder rungs must be in good condition and the first rung must not be higher than 18\" above the ground." chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:29 name:@"Are guards and enclosures in place and in good condition?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:30 name:@"Casing and tubing valves are operational and in proper position?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:31 name:@"Confirm that casing valves are not buried." chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:32 name:@"Are all open ended valves plugged or capped?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:33 name:@"Pressure rating of guages correct for service?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:34 name:@"Is well cellar free of liquid and debri?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:35 name:@"Can cellar be backfilled?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:36 name:@"Are lube oil or chemical drumcontainment areas maintained and able to contain 110% of the largest drum?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:37 name:@"Is stuffing box leaking?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:38 name:@"Are there any visible signs of leakage in piping?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:39 name:@"Have oilspills and drilling /well work over materials been cleaned up?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:40 name:@"Is location clear of weeds and unused oilfield equipment and piping?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:41 name:@" Weeds cleared 15 ft around power panels?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:42 name:@"Do steam wells have proper tie downs and precautions for expansion? " chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:43 name:@"Is polish rod pitted or grooved?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:44 name:@"Are there any visual signs of leakage on bearings or gearbox of pumping unit?" chemicalType:@"DEFAULT"];
	[oilWellDB insertDataLookupChemicalProperties:45 name:@"Throat bolt or flag pin installed on horsehead of pumping unit?" chemicalType:@"DEFAULT"];
    
}

@end
