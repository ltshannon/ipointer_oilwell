//
//  DataManager.m
//  GoGoCabi
//
//  Created by Vitaly Dubov on 11/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import "DataManager.h"
#import "Constants.h"

@implementation DataManager

@synthesize dict;

static DataManager *sharedInstance;

- (void) dealloc {
    [dict release];
    [super dealloc];
}

+ (DataManager *)sharedInstance {
    @synchronized(self) {
        if (!sharedInstance)
            sharedInstance = [[DataManager alloc] init];       
    }
    return sharedInstance;
}

+(id)alloc {
    @synchronized(self) {
        NSAssert(sharedInstance == nil, @"Attempted to allocate a second instance of a singleton DataManager.");
        sharedInstance = [super alloc];
    }
    return sharedInstance;
}

-(id) init {
    if (self = [super init]) {
        self.dict = [NSMutableDictionary dictionary];
    }
    return self;
}

- (void) setObject: (id) object forKey: (NSString *) key {
    if (key) {
        if (object)
            [dict setObject:object forKey:key];
        else
            [self clearKey:key];
    }
}

- (id) objectForKey: (NSString *) key {
    return [dict objectForKey:key];
}

- (void) clear {
    [dict removeAllObjects];
}

- (void) clearKey: (NSString *) key {
    [dict removeObjectForKey:key];
}


- (BOOL) isSessionActive {
    return [self objectForKey:kSessionTokenDataKey] != nil;
}

- (void) clearRememberedCredentials {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kRememberedEmailDataKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kRememberedPasswordDataKey];
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:kRememberedRememberFlagDataKey];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

@end
