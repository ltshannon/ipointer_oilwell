//
//  coreLocationController.h
//  realtyPointerMobile3
//
//  Created by Daniel Johnson on 6/12/11.
//  Copyright 2011 Balance Rock Media. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <CoreLocation/CoreLocation.h>
#import <CoreMotion/CoreMotion.h>

//#define DEVLOCATION
//#define DEVHEADING

extern NSString * const NOTIF_LocationOK;
extern NSString * const NOTIF_LocationNotOK;

@interface CoreLocationController : NSObject <CLLocationManagerDelegate> {
    CLLocationManager *locationManager;
    CMMotionManager *motionManager;
	NSMutableString *urlString;
	NSMutableString *latitude;
	NSMutableString *longitude;
	NSMutableString *altitude;
	NSString *m_CarrierName;
}
@property (nonatomic, retain) NSMutableString *urlString;
@property (nonatomic, retain) CLLocationManager *locationManager;
@property (nonatomic, retain) CMMotionManager *motionManager;

@property (nonatomic, retain) NSMutableString *latitude;
@property (nonatomic, retain) NSMutableString *longitude;
@property (nonatomic, retain) NSMutableString *altitude;

@property (nonatomic, assign) CLLocationDirection heading;// changed
@property (nonatomic, assign) BOOL locationAvailable;

- (void)createLocation;
- (void)startUpdatingLocation;
- (NSString *) pitch;
- (NSMutableString *) getUrlString;
//- (NSArray *)getLatLong;
- (float)gethorizontalAccuracy;
- (double)degrees:(double)radians;

+(CoreLocationController*)sharedInstance;

@end
