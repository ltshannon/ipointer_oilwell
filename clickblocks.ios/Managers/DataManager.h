//
//  DataManager.h
//  GoGoCabi
//
//  Created by Vitaly Dubov on 11/9/11.
//  Copyright (c) 2011 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface DataManager : NSObject {
    NSMutableDictionary *dict;
}

@property (nonatomic, retain) NSMutableDictionary *dict;

+ (DataManager *) sharedInstance;
- (void) setObject: (id) object forKey: (NSString *) key;
- (id) objectForKey: (NSString *) key;
- (void) clear;
- (void) clearKey: (NSString *) key;

- (BOOL) isSessionActive;
- (void) clearRememberedCredentials;

@end
