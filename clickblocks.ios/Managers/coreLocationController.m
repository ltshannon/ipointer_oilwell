
//  coreLocationController.m
//  realtyPointerMobile3
//
//  Created by Daniel Johnson on 6/12/11.
//  Copyright 2011 Balance Rock Media. All rights reserved.
//

#import "coreLocationController.h"
#import "Constants.h"
#import "Common.h"
#import "AppDelegate.h"
#import "ViewController.h"

NSString * const NOTIF_LocationOK = @"LocationOK";
NSString * const NOTIF_LocationNotOK = @"LocationNotOK";

static CoreLocationController *sharedCoreLocationManager = nil;


@implementation CoreLocationController


NSTimeInterval howRecent;

@synthesize locationManager,urlString;
@synthesize motionManager;

@synthesize latitude;	
@synthesize longitude;
@synthesize altitude;

@synthesize heading;

float horizontalAccuracy;

+ (CoreLocationController*)sharedInstance
{
    @synchronized(sharedCoreLocationManager)
    {
        if (!sharedCoreLocationManager || sharedCoreLocationManager == nil)
        {
            sharedCoreLocationManager = [[CoreLocationController alloc] init];
        }
        return sharedCoreLocationManager;
    }
}

+ (id)allocWithZone:(NSZone *)zone
{
    @synchronized(self)
    {
        if (sharedCoreLocationManager == nil)
        {
            sharedCoreLocationManager = [super allocWithZone:zone];
            return sharedCoreLocationManager;
        }
    }
    return nil;
}

- (void)createLocation 
{
    self.locationManager = [[[CLLocationManager alloc] init] autorelease];
    self.locationManager.desiredAccuracy = kCLLocationAccuracyBestForNavigation;
    self.locationManager.distanceFilter = 1;
    self.locationManager.delegate = self;
    self.locationManager.headingOrientation = CLDeviceOrientationFaceUp;
    self.motionManager = [[[CMMotionManager alloc] init] autorelease];
}

#pragma mark -
#pragma mark - Location

- (void)startUpdatingLocation {
    // location
    [CLLocationManager locationServicesEnabled];
    [locationManager startUpdatingLocation];
    [locationManager startUpdatingHeading];
	
	// motion
    if (self.motionManager.deviceMotionAvailable) {
        self.motionManager.deviceMotionUpdateInterval = 1.0 / 60.0;
        [motionManager startDeviceMotionUpdates];            
    } else {
        [motionManager startAccelerometerUpdates];   
    }
}

- (NSString *) pitch 
{
    return @"0";
    
#ifdef DEVLOCATION
    return @"0.0";
#else
    if (motionManager.deviceMotionAvailable) {
        return [NSString stringWithFormat:@"%f", [self degrees:self.motionManager.deviceMotion.attitude.pitch]];
    } else {
        return [NSString stringWithFormat:@"%f", self.motionManager.accelerometerData.acceleration.y * -90.0];
    }
#endif
}

- (NSMutableString *) altitude
{
    return [NSMutableString stringWithString:@"1.5"];
}

- (NSMutableString *) getUrlString
{
	abort(); //remove this function entirely	
	//urlString = [NSMutableString stringWithString:@"http://50.57.89.107/services/identify/?lng=-119.095813&lat=35.322859&h=0.0&azi=60&elv=0"];
	
//	    urlString = [NSMutableString stringWithString:@"http://50.57.89.107/services/identify/?"];
    urlString = [NSMutableString stringWithString:@"http://realtypointer.saritasa.com/index.php?"];
    
    [urlString appendFormat:@"lat=%@", self.latitude];
    [urlString appendFormat:@"&lng=%@", self.longitude];
    [urlString appendFormat:@"&h=%@", self.altitude];
    [urlString appendFormat:@"&azi=%f", self.heading];
    [urlString appendFormat:@"&elv=%@", [self pitch] ];
  
     DLog(@"urlString: %@", urlString);
    
    return urlString;
}

- (CLLocationDirection) heading {    
#ifdef DEVHEADING
    return 15.0;
#else
    
    if ([Common appDelegate].viewController.interfaceOrientation == UIInterfaceOrientationLandscapeLeft) {
        DLog(@"UIInterfaceOrientationLandscapeLeft");
        
        if (heading >= 90)
            return heading - 90;
        
        return 360 - (90 - heading);
    } else if ([Common appDelegate].viewController.interfaceOrientation == UIInterfaceOrientationLandscapeRight) {
        DLog(@"UIInterfaceOrientationLandscapeRight");
        
        if (heading <= 270)
            return heading + 90;
        
        return heading + 90 - 360;
    }
    
    return heading;
#endif
}

- (float)gethorizontalAccuracy
{
    return horizontalAccuracy;
}

#pragma mark - Location Delegate Methods

- (void)locationManager:(CLLocationManager *)manager didUpdateToLocation:(CLLocation *)newLocation
    fromLocation:(CLLocation *)oldLocation
{
    DLog(@"didUpdateToLocation");
    NSDate *newEventDate = newLocation.timestamp; // to calculate timestamp
	NSDate *oldEventDate = oldLocation.timestamp;
	howRecent = [newEventDate timeIntervalSinceDate:oldEventDate];
	
	//CTTelephonyNetworkInfo *tmp_Telephony = [[CTTelephonyNetworkInfo alloc] init];
	//CTCarrier *carrier = [tmp_Telephony subscriberCellularProvider];
	//m_CarrierName=[carrier carrierName];
	//[tmp_Telephony release];
	
	horizontalAccuracy = newLocation.horizontalAccuracy;
#ifdef DEVLOCATION
    //    lng=-118.975133&lat=35.383758
//    self.latitude = [[@"35.383758" mutableCopy] autorelease];
//    self.longitude = [[@"-118.975133" mutableCopy] autorelease];
    
    
    
    self.longitude = [[@"-119.064432" mutableCopy] autorelease];
    self.latitude = [[@"35.313269" mutableCopy] autorelease];
    
    
    
    //    self.latitude = [[@"34.063865" mutableCopy] autorelease];
    //    self.longitude = [[@"-118.325969" mutableCopy] autorelease];
    //    self.latitude = [[@"35.383758" mutableCopy] autorelease];
    //    self.longitude = [[@"-119.095813" mutableCopy] autorelease];
    
    self.altitude = [NSMutableString stringWithFormat:@"1.5"];
#else
    self.latitude = [NSMutableString stringWithFormat:@"%f", newLocation.coordinate.latitude];
    self.longitude = [NSMutableString stringWithFormat:@"%f", newLocation.coordinate.longitude];
    self.altitude = [NSMutableString stringWithFormat:@"%f", newLocation.altitude];
#endif
	
	DLog(@"lat = %@", latitude);
	DLog(@"long = %@", longitude);
//    self.longitude = [@"-119.095813" mutableCopy];
    
//	NSLog(@"lat:- %f", latitude);
//	NSLog(@"long:- %f", longitude);
    
    // set to 200 for testing purposes, change to 10 for real application
	//
//	if ([m_CarrierName  isEqualToString:@"Verizon"]) // for cdma devices 
    if (NO) //temporary remove this code branch
	{
		DLog(@"Entered in the loop");
		if (newLocation.horizontalAccuracy <= 1500 &&newLocation.verticalAccuracy<=24&&howRecent<=10.267083) 
		{	
			DLog(@"Accuracy OK: %@", [NSString stringWithFormat:@"%0.0f", newLocation.horizontalAccuracy]);
			
			[[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_LocationOK object:nil];
						
		} 
		else 
		{
			[locationManager startUpdatingLocation];
			
		}
	}
	else 
	{
	    if (newLocation.horizontalAccuracy <= 1000)
        {
            DLog(@"Accuracy OK: %@", [NSString stringWithFormat:@"%0.0f", newLocation.horizontalAccuracy]);
            _locationAvailable = YES;
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_LocationOK object:nil];
        } 
		else 
		{
            DLog(@"Accuracy too low: %@", [NSString stringWithFormat:@"%0.0f", newLocation.horizontalAccuracy]);
            [[NSNotificationCenter defaultCenter] postNotificationName:NOTIF_LocationNotOK object:nil];
		}
	}
}



-(void)startUpdatingHeading
{
	DLog(@"started updating heading");
}
- (void)locationManager:(CLLocationManager *)manager didUpdateHeading:(CLHeading *)newHeading
{
	NSLog(@"new heading\n");
    self.heading = newHeading.trueHeading;
    
    DLog(@"\ntrue heading: %f\nmagnetic heading: %f", newHeading.trueHeading, newHeading.magneticHeading);
}
	
- (void)locationManager:(CLLocationManager *)manager didFailWithError:(NSError *)error
{
	DLog(@"Location Manager Error: %@", [error description]);
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Location Manager Error" message:[error description] delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
    [alert show];

}

#pragma mark -
#pragma mark - Helpers

- (double)degrees:(double)radians
{
    return radians / M_PI * 180;
}

- (void)dealloc
{
    [locationManager release];
    
    [super dealloc];
}

@end

