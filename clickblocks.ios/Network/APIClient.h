//
//  APIClient.h
//  MyPersonalBest
//
//  Created by Vitaly Dubov on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "AFNetworking.h"

extern NSString* const kDefaultApiBaseURL;

extern NSString* const kHTTPMethodGET;
extern NSString* const kHTTPMethodPOST;
extern NSString* const kHTTPMethodPUT;
extern NSString* const kHTTPMethodDELETE;

extern NSString* const kApiKeyLogin;
extern NSString* const kApiKeyWellDataByPoint;
extern NSString* const kApiKeyWellDataByAPINumber;
extern NSString* const kApiKeyAudit;
extern NSString* const kApiKeySaveAudit;

extern NSString* const kApiErrorDomain;

#define kApiErrorCodeUnknown 1
#define kApiErrorCodeUnexpectedData 2

@interface APIClient : AFHTTPClient

+ (id) sharedInstance;

// override to implement custom error checking
- (NSError *) checkError:(id)response;
- (NSError *) checkError:(id)response apiKey:(NSString *)apiKey parameters:(NSDictionary *)parameters;

// override to define API path or full url string for given API name (key) and parameters
- (NSString *) apiPathForKey:(NSString *)apiKey parameters:(NSDictionary *)parameters;

// override to change parameters before request, i.e. to add some common params like session key or hash etc.
- (NSMutableDictionary *) modifiedParametersForKey:(NSString *)apiKey parameters:(NSDictionary *)parameters;

- (void) callApi:(NSString *)apiKey httpMethod: (NSString *) httpMethod parameters:(NSDictionary*)parameters success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock;

- (void) loginWithEmail: (NSString *) email password: (NSString *) password completion: (void(^)(NSError *error)) completion;

@end
