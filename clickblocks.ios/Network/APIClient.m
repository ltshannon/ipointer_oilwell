//
//  APIClient.m
//  MyPersonalBest
//
//  Created by Vitaly Dubov on 7/30/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "APIClient.h"
#import "Constants.h"
#import "DataManager.h"
#import "JSONKit.h"
#import "AppDelegate.h"


NSString* const kApiErrorDomain = @"oilwell";

// LTS 9/24/2013
//NSString* const kDefaultApiBaseURL = @"http://oilwell.saritasa.com/api/";
NSString* const kDefaultApiBaseURL = @"http://quisillic.com/api/";

NSString* const kHTTPMethodGET = @"GET";
NSString* const kHTTPMethodPOST = @"POST";
NSString* const kHTTPMethodPUT = @"PUT";
NSString* const kHTTPMethodDELETE = @"DELETE";

NSString* const kApiKeyLogin = @"user/login";
NSString* const kApiKeyWellDataByPoint = @"well/getByPoint";
NSString* const kApiKeyWellDataByAPINumber = @"well/getByAPI";
NSString* const kApiKeyAudit = @"well/getAudit";
NSString* const kApiKeySaveAudit = @"well/saveAudit";

@implementation APIClient

+ (id)sharedInstance {
    static APIClient *__sharedInstance;
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        __sharedInstance = [[APIClient alloc] initWithBaseURL:[NSURL URLWithString:kDefaultApiBaseURL]];
    });
    
    return __sharedInstance;
}

- (id)initWithBaseURL:(NSURL *)url {
    self = [super initWithBaseURL:url];
    if (self) {
        //custom settings
        [self registerHTTPOperationClass:[AFJSONRequestOperation class]];
        [AFHTTPRequestOperation addAcceptableStatusCodes:[NSIndexSet indexSetWithIndexesInRange:NSMakeRange(100, 500)]];
    }
    
    return self;
}

- (NSError *) checkError: (id) response {
    return [self checkError:response apiKey:nil parameters:nil];
}

- (NSError *) checkError:(id)response apiKey:(NSString *)apiKey parameters:(NSDictionary *)parameters {
    if (![response isKindOfClass:[NSDictionary class]]) {
        return [NSError errorWithDomain:kApiErrorDomain code:kApiErrorCodeUnexpectedData userInfo:[NSDictionary dictionaryWithObject:@"Unexpected data" forKey:NSLocalizedDescriptionKey]];
    } else {
        NSDictionary *dict = (NSDictionary *)response;
        if (![[dict objectForKey:@"success"] boolValue]) {
            NSString *errorDesc = [[dict objectForKey:@"error"] objectForKey:@"description"];
            NSInteger errorCode = [[[dict objectForKey:@"error"] objectForKey:@"code"] intValue];
            
//            if (errorCode == 404) {
//                errorDesc = [NSString stringWithFormat:@"We currently do not have any informations about API number: %@ in our database.", [parameters objectForKey:@"API"]];
//            }
            
            NSDictionary *userInfo = errorDesc ? [NSDictionary dictionaryWithObject:errorDesc forKey:NSLocalizedDescriptionKey] : nil;
            
            return [NSError errorWithDomain:kApiErrorDomain code:errorCode userInfo:userInfo];
        }
    }
    return nil;
}

- (NSString *) apiPathForKey:(NSString *)apiKey parameters:(NSDictionary *)parameters {
    return apiKey;
}

- (NSMutableDictionary *) modifiedParametersForKey:(NSString *)apiKey parameters:(NSDictionary *)parameters {
    NSMutableDictionary *dict = [NSMutableDictionary dictionaryWithDictionary:parameters];
    
    NSString *sKey = [[DataManager sharedInstance] objectForKey:kSessionTokenDataKey];
    if (sKey) {
        [dict setObject:sKey forKey:@"sKey"];
    }
    
    return dict;
}

- (void) callApi:(NSString *)apiKey httpMethod: (NSString *) httpMethod parameters:(NSDictionary*)parameters success:(void (^)(AFHTTPRequestOperation *operation, id responseObject))successBlock failure:(void (^)(AFHTTPRequestOperation *operation, NSError *error))failureBlock{
    NSString *apiPath = [self apiPathForKey:apiKey parameters:parameters];
    NSDictionary *params = [self modifiedParametersForKey:apiKey parameters:parameters];
    params = [NSMutableDictionary dictionaryWithObject:[params JSONString] forKey:@"data"];
    AppDelegate *appD = (id) [[UIApplication sharedApplication] delegate];
    
    NSComparisonResult result = [[NSDate date] compare: [[DataManager sharedInstance] objectForKey:kSessionTimeout]];
    if (![apiKey isEqualToString:kApiKeyLogin] && result != NSOrderedAscending)
    {

// LTS 11/4/2013 - Try to re-login if timeout
//        UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Your session has timed out, restart the application." message:nil delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil];
//        [alert show];
        appD.loginViewController.modalPresentationStyle = UIModalPresentationFullScreen;
        [appD.viewController presentViewController:appD.loginViewController animated:NO completion:nil];
        return;
    }
    
    DLog(@"BASE: %@, apiPath start %@",[self.baseURL absoluteString], apiPath);

    NSDate * start = [NSDate date];
    
    void(^_success)(AFHTTPRequestOperation*, id) = ^(AFHTTPRequestOperation *operation, id responseObject)
    {
        NSString *timeString = [NSString stringWithFormat:@"Call: %@ took: %f", apiKey, -[start timeIntervalSinceNow]];
        [appD.timeStrings addObject:timeString];
        NSLog(@"Time took: %f for Call: %@", -[start timeIntervalSinceNow], apiKey);
        DLog(@"apiPath finished %@", apiPath);
        DLog(@"response = %@", responseObject);
        NSError *error = [self checkError:responseObject apiKey:apiKey parameters:parameters];
        if (error) {
            DLog(@"%@", error);
            failureBlock(operation, error);
        } else {
            successBlock(operation, responseObject);
        }
    };
    
    void(^_failure)(AFHTTPRequestOperation*, NSError*) = ^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"apiPath failed %@", apiPath);
        DLog(@"%@", error);
        failureBlock(operation, error);
    };
    
    if ([httpMethod isEqualToString:kHTTPMethodGET]) {
        [self getPath:apiPath parameters:params success:_success failure:_failure];
    } else if ([httpMethod isEqualToString:kHTTPMethodPOST]) {
        [self postPath:apiPath parameters:params success:_success failure:_failure];
    } else if ([httpMethod isEqualToString:kHTTPMethodPUT]) {
        [self putPath:apiPath parameters:params success:_success failure:_failure];
    } else if ([httpMethod isEqualToString:kHTTPMethodDELETE]) {
        [self deletePath:apiPath parameters:params success:_success failure:_failure];
    }
}

- (void) loginWithEmail: (NSString *) email password: (NSString *) password completion: (void(^)(NSError *error)) completion
{

    NSMutableDictionary *params = [NSMutableDictionary dictionary];
    if (email)
        [params setObject:email forKey:@"username"];
    if (password)
        [params setObject:password forKey:@"password"];
    
    [[DataManager sharedInstance] clear];
    
    [self callApi:kApiKeyLogin httpMethod:kHTTPMethodGET parameters:params success:^(AFHTTPRequestOperation *operation, id responseObject) {
        DLog(@"request completed %@", operation.response.URL);
        DLog(@"%@", responseObject);
        
        NSString *token = [[responseObject objectForKey:@"result"] objectForKey:@"sKey"];
        [[DataManager sharedInstance] setObject:token forKey:kSessionTokenDataKey];
        NSString *sessionTime = [[responseObject objectForKey:@"result"] objectForKey:@"sessionLifeTime"];
        [[DataManager sharedInstance] setObject:[[NSDate date] dateByAddingTimeInterval:[sessionTime intValue]] forKey:kSessionTimeout];
// Use for test for timeout        [[DataManager sharedInstance] setObject:[NSDate date] forKey:kSessionTimeout];
        [[DataManager sharedInstance] setObject:email forKey:kSessionUserName];
        [[DataManager sharedInstance] setObject:password forKey:kSessionPassword];
        
        [self setAuthorizationHeaderWithToken:token];
        
        NSLog(@"Time Out: %@", [[DataManager sharedInstance] objectForKey:kSessionTimeout]);
        
        completion(nil);
    } failure:^(AFHTTPRequestOperation *operation, NSError *error) {
        DLog(@"request failed %@", operation.response.URL);
        DLog(@"error = %@", error);
        
        [[DataManager sharedInstance] clearKey:kSessionTokenDataKey];
        [self clearAuthorizationHeader];
        
        completion(error);
    }];
 
 //   completion(NO);
}

@end
